# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
September 2023

Authors:
- Leandro Gimmi

Description:
Extract three independent precipitation maxima for individual
 CORDEX-FPS or EUROCORDEX models, regions and seasons. Dry areas (>15 maxima 
 are <1mm/h) are excluded. This method was proposed in Ban et al. 2018 
 (DOI: 10.1007/s00382-018-4339-4). This script uses gridded model data stored 
 as ncfile as input, returns .npy file which will be remade into a .nc file at
 a later stage of dataprocessing in another script

"""

import os, sys
import glob
import time
import warnings
import numpy as np
import xarray as xr 
import cartopy.crs as ccrs
from natsort import natsorted
import matplotlib.pyplot as plt
from multiprocessing import Pool
from matplotlib.pyplot import cm
import cartopy.feature as cfeature

'''-------------------------------------------------------------------------'''
### ----------------------------------------------------------------------- ###
###     SET YOUR PARAMETRES HERE
### ----------------------------------------------------------------------- ###

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'evaluation'

### 1hr, 3hr, 6hr, 1d, 3d, 5d
time_res = '1hr'

## Seasons: DJF, MAM, JJA, SON
seasons = ['DJF', 'MAM', 'JJA', 'SON']

## CH2018 regions: CHNE, CHW, CHS, CHAE, CHAW or CH (all of Switzerland)
regions = ['CH','CHNE', 'CHW', 'CHS', 'CHAE', 'CHAW']

### ----------------------------------------------------------------------- ###
###     SET YOUR DATA PATHS HERE AND SELECT MODELS (CPM [rcm=None]
###      or RCM [rcm=1])
### ----------------------------------------------------------------------- ###

### - - - - - - - - - - - - - - - CPM - - - - - - - - - - - - - - - - - - - ###
path_prdata = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr/{scenario}'
path_regions = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
plot_path = '/users/lgimmi/MeteoSwiss/figures'
path_save = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_maxima/'
             +f'{scenario}')

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15',
        'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME',
        'HadREM3-RA-UM10.1','RegCM4-7']

rcm = None


### - - - - - - - - - - - - - - - RCM - - - - - - - - - - - - - - - - - - - ###
# path_prdata = f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr/{scenario}'
# path_regions = '/scratch/snx3000/lgimmi/store/rcm/masks'
# plot_path = '/users/lgimmi/MeteoSwiss/figures'
# path_save = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_maxima/'
#              +f'{scenario}')

# if scenario == 'evaluation' and time_res != '1d':
#     Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
# elif scenario == 'evaluation' and time_res == '1d':
#     Institutes = ['CLMcom-ETH','CLMcom-BTU','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-9','ALADIN62','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
# else:
#     Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']

# rcm = 1

'''-------------------------------------------------------------------------'''
def overview_plot(da, values):
    # '''-----------------------------------------------------------'''
    # ## Quick plot to check if putting Dataset together worked
    # '''-----------------------------------------------------------'''
    ### Most models are in rotated lat lon grid
    rlat,rlon,pole = da.rlat, da.rlon, da.rotated_pole
 
    try:
        if hasattr(pole,'grid_north_pole_longitude'):
            pole_lon = pole.attrs['grid_north_pole_longitude']
        if hasattr(pole,'grid_north_pole_latitude'):
            pole_lat = pole.attrs['grid_north_pole_latitude']
    except:
        print('Unexpected error:', sys.exc_info()[0])
        raise
    crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

    # map_ext = np.array([-180, 180, -90, 90])  # [degree]
    map_ext = np.array([-0, 11, 45, 49])  # [degree]
    # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
    rad_earth = 6371.0  # approximate radius of Earth [km]
    dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
        map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
    dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

    fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                        subplot_kw={'projection':ccrs.PlateCarree()})
    # fig, ax = plt.subplots(figsize=(12.0 , 12.0),
    # subplot_kw={'projection':ccrs.PlateCarree()})
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
    ax.set_aspect("auto")
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.contourf(rlon, rlat, values, cmap='jet', transform=crs)
    
    plt.show()
    breakpoint()

if scenario == 'evaluation':
    Institutes.insert(0,'obs')
    CPMs.insert(0,'obs')

for region in regions:
    for season in seasons:
        ### Assign number found in mask-file to region
        CHNE, CHW, CHS ,CHAE, CHAW, CH = 1, 2, 3, 4, 5, 1
        myVars = locals()
        i_region = myVars[region]
        if scenario == 'evaluation':
            start_year = 2000
        elif scenario == 'historical':
            start_year = 1996
        elif scenario == 'rcp85_moc':
            start_year = 2040
            path_prdata = path_prdata[:-4] + '/moc'
            path_save = path_save[:-4] + '/moc'
        elif scenario == 'rcp85_eoc':
            start_year = 2090
            path_prdata = path_prdata[:-4] + '/eoc'
            path_save = path_save[:-4] + '/eoc'

        for i_cpm, cpm in enumerate(Institutes):
            if cpm == 'obs':
                continue
            print((f'Scenario: {scenario}, Region: {region}, Model: {cpm}, '
                   + f'Time res: {time_res}, Season: {season}').center(50,'-'))
            
            maxima_ten_year = []
            check_ind_max_next_year_begin = False
            ### Get filenames
            if cpm == 'KNMI':
                filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
            elif cpm == 'MOHC':
                filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
            elif cpm == 'CLMcom-KIT' and scenario == 'evaluation':
                filename = (path_prdata + '/' + f'*{cpm}**CCLM5-0-14*')
            elif (cpm == 'HCLIMcom' and scenario == 'evaluation' and 
                  time_res == '1d'):
                filename = (path_prdata + '/' + f'*{cpm}**{CPMs[i_cpm]}*_1d_*')
            else:
                filename = (path_prdata + '/' + f'*{cpm}**{CPMs[i_cpm]}*')
            filenames_list = natsorted(glob.glob(filename))

            ### Look at each year individually and select season
            if season == 'DJF':
                range_num = 11
                ### ETH RCM only has 9 years of data in some scenarios
                if (rcm == 1 and cpm == 'CLMcom-ETH' and
                     scenario in ['evaluation','rcp85_eoc']):
                    range_num = 10
                if (rcm == 1 and time_res == '1d'):
                    range_num = 10
                if (cpm == 'CLMcom-ETH' and time_res == '1d'):
                    range_num = 10
            else:
                range_num = 10
                ### ETH RCM only has 9 years of data in some scenarios
                if (rcm == 1 and cpm == 'CLMcom-ETH' and
                     scenario in ['evaluation','rcp85_eoc']):
                    range_num = 9
                elif (rcm == 1 and time_res == '1d'):
                    range_num = 9
                if (cpm == 'CLMcom-ETH' and time_res == '1d'):
                    range_num = 9

            for i_year in range(range_num):
                if cpm == 'obs':
                    print(f'Year: {start_year + 5 + i_year}')
                elif season == 'DJF':
                    print(f'Years: {start_year + i_year - 1}/'
                          + f'{start_year + i_year}')
                else:
                    print(f'Year: {start_year + i_year}')
                warnings.simplefilter("ignore")
                if season == 'DJF':
                    if i_year == 0:
                        ### MOHC has individual months as input files
                        if cpm == 'MOHC':
                            filenames = filenames_list[12*i_year:12+12*i_year]
                            ds = xr.open_mfdataset(filenames[:2],
                                                    use_cftime=True)
                            da = ds['pr']
                            ds.close()
                        else:
                            filenames = filenames_list[i_year]
                            if cpm == 'obs':
                                ds = xr.open_dataset(filenames)
                                da = ds['pr']
                                da = da.groupby("REFERENCE_TS.month")
                                da_jan, da_feb = da[1], da[2]
                                da = xr.concat([da_jan,da_feb],
                                               dim='REFERENCE_TS')
                                ds.close()
                            else:
                                ds = xr.open_mfdataset(filenames,
                                                        use_cftime=True)
                                da = ds['pr']
                                da = da.groupby("time.month")
                                da_jan, da_feb = da[1], da[2]
                                da =xr.concat([da_jan,da_feb],dim='time')
                                ds.close()
                    elif i_year == (range_num-1):
                        if cpm == 'MOHC':
                            filenames = \
                                filenames_list[12*(i_year-1):12+12*(i_year-1)]
                            ds = xr.open_mfdataset(filenames[11],
                                                    use_cftime=True)
                            da = ds['pr']
                            ds.close()
                        else:
                            filenames = filenames_list[i_year-1]
                            if cpm == 'obs':
                                ds = xr.open_dataset(filenames)
                                da = ds['pr']
                                da = da.groupby("REFERENCE_TS.month")
                                da = da[12]
                                ds.close()
                            else:
                                ds = xr.open_mfdataset(filenames,
                                                        use_cftime=True)
                                da = ds['pr']
                                da = da.groupby("time.month")
                                da = da[12]
                                ds.close()
                        
                    else:
                        if cpm == 'MOHC':
                            filenames = \
                                filenames_list[12*i_year-12:12+12*i_year]
                            ds = xr.open_mfdataset(filenames[11:14],
                                                    use_cftime=True)
                            da = ds['pr']
                            ds.close()
                        else:
                            filenames = filenames_list[i_year-1:i_year+1]

                            ### open 2y (D-JF) separately to reduce memory load
                            if cpm == 'obs':
                                ds = xr.open_dataset(filenames[0])
                                da = ds['pr']
                                da = da.groupby("REFERENCE_TS.month")
                                da_dec = da[12]
                                ds.close()
                                ds = xr.open_mfdataset(filenames[1])
                                da = ds['pr']
                                da = da.groupby("REFERENCE_TS.month")
                                da_jan, da_feb = da[1], da[2]
                                ds.close()
                                da =xr.concat([da_dec,da_jan,da_feb],
                                            dim='REFERENCE_TS')
                            else:
                                ds = xr.open_mfdataset(filenames[0],
                                                        use_cftime=True)
                                da = ds['pr']
                                da = da.groupby("time.month")
                                da_dec = da[12]
                                ds.close()
                                ds = xr.open_mfdataset(filenames[1],
                                                        use_cftime=True)
                                da = ds['pr']
                                da = da.groupby("time.month")
                                da_jan, da_feb = da[1], da[2]
                                ds.close()
                                da = xr.concat([da_dec,da_jan,da_feb],
                                              dim='time')
                else:
                    if cpm == 'MOHC':
                        filenames = filenames_list[12*i_year:12+12*i_year]
                    else:
                        filenames = filenames_list[i_year]

                    if cpm == 'obs':
                        ds = xr.open_dataset(filenames)
                        da = ds['pr']
                        da = da.groupby("REFERENCE_TS.season")
                        da = da[season]
                        ds.close()
                    else:
                        ds = xr.open_mfdataset(filenames, use_cftime=True)
                        da = ds['pr']
                        da = da.groupby("time.season")
                        da = da[season]
                        ds.close()
                ### open mask file for regions, for all of Switzerland it is
                ### a different one than for CH2018 Regions
                if region == 'CH':
                    endname = 'maskCH'
                    variable_mask = 'mask_CH'
                else:
                    endname = 'Ch2018'
                    variable_mask = 'orog'

                ### Some models need differently shaped masks for different
                ### time periods
                if cpm == 'MOHC':
                    region_file = (path_regions+ f'/*{CPMs[i_cpm]}*{endname}*')
                elif cpm == 'CNRM' and scenario == 'historical' and rcm == 1:
                    region_file = (path_regions + 
                                   f'/{cpm}*{CPMs[i_cpm]}*{endname}*')
                elif (cpm == 'ETH' and scenario == 'evaluation' and rcm == 1 
                      and time_res == '1d'):
                    region_file = (path_regions + 
                                   f'/ETH_oneday_*{endname}*')
                else:
                    region_file = (path_regions + 
                                   f'/*{cpm}*{CPMs[i_cpm]}*{endname}*')
                ds_region = xr.open_mfdataset(region_file)
                mask_region = ds_region[variable_mask]
                ds_region.close()

                ### Allign masks to dimensions and cooridinates of CPM as they
                ###  differ slightly on the 7th or higher decimal place. I 
                ### assume this happend because not all models and masks were 
                ### regridded with the same method (python/cdo, billinear/nn)
                mask_region = \
                    mask_region.expand_dims(dim={"time": len(da.time)})
                mask_region = mask_region.assign_coords(time=da.time)
                try:
                    da,mask_region = xr.align(da, mask_region, join="override")    
                except:
                    print(f'{cpm} has not alligned with its mask')
                    pass

                # '''-----------------------------------------------------'''
                ### Diff. way to mask data (extent outside region is kept)
                ### Separate masking into blocks of size limited to 1.0GB
                ### to avoid freezing of code due to memory issues
                ### Used for CMCC model as it has coordinate problem
                # '''-----------------------------------------------------'''

                def masking(xarray_dataarray,  mask, max_block = 1.0):
                    '''
                    Separate masking into blocks of size limited to 1.0GB 
                    (max_block) to avoid freezing of code due to memory issues

                    -----
                    Returns maked input array as numpy array

                    '''
                    global i_region

                    block_size = max_block
                    da = xarray_dataarray
                    mask_region = mask
                    block_size = 1.0
                    
                    len_time = da['time'].size
                    if ('x' in list(da.coords)) and ('y' in list(da.coords)):
                        len_x = da['x'].size
                        len_y = da['y'].size
                        out_dim = {'y','x'}
                    elif (('rlon' in list(da.coords)) and 
                            ('rlat' in list(da.coords))):
                        len_x = da['rlon'].size
                        len_y = da['rlat'].size
                        out_dim = {'rlat','rlon'}
                    elif (('lon' in list(da.coords)) and 
                          ('lat' in list(da.coords))):
                        len_x = da['lat'].size
                        len_y = da['lon'].size
                        out_dim = {'lon','lat'}
                    else:
                        print(f'{cpm} uses an unknown coordinate systems'
                            +f' -- {list(da.coords)} -- ')
                    # ### For different time resolutions this model has different
                    # ### coordinate names
                    # if cpm == 'CLMcom-CMCC' and time_res in ['1hr','3hr','6hr']:
                    #     len_x = ds['rlon'].size
                    #     len_y = ds['rlat'].size
                    #     out_dim = {'rlat','rlon'}
                    # elif cpm == 'CLMcom-CMCC' and time_res in ['1d','3d','5d']:
                    #     breakpoint()
                    #     len_x = ds['x'].size
                    #     len_y = ds['y'].size
                    #     out_dim = {'rlat','rlon'}

                    dataarray = np.empty((len_time, len_y, len_x),
                                          dtype=np.float32)
                    dataarray.fill(np.nan)
                    maskarray = np.empty((len_time, len_y, len_x),
                                          dtype=np.float32)
                    maskarray.fill(np.nan)
                    da_masked = np.empty((len_time, len_y, len_x),
                                          dtype=np.float32)
                    da_masked.fill(np.nan)

                    num_blocks = int(np.ceil((da_masked.nbytes / (10 ** 9)) / 
                                             block_size))
                    lim = np.linspace(0, len_time, num_blocks + 1,
                                       dtype=np.int32)  
                    
                    for i in range(num_blocks):
                        t_beg = time.time()
                        slice_t = slice(lim[i], lim[i + 1])
                        dataarray[slice_t,:,:] = da[slice_t,:,:].values
                        maskarray[slice_t,:,:] = mask_region[slice_t,:,:].values

                        da_masked[slice_t,:,:] = np.where(
                            maskarray[slice_t,:,:] == float(i_region),
                            dataarray[slice_t,:,:] * multiply, # change units to mm/hr
                            0.0
                            )
                        
                        print('Data blocks loaded: ' + str(i + 1) + '/' + 
                              str(num_blocks))
                        print('Time for masking for this block: ' + 
                            '%.2f' % (time.time() - t_beg) + ' s')
                    return da_masked

                # '''-------------------------------------------------------'''
                ### Masking using xarray can drop unnecessairy grid cells to 
                ### speed up code and free up memory (Not working with CMCC 1h)
                # '''-------------------------------------------------------'''   
                
                ### CHECK units of dataset and check wheter values need to be
                ### adjusted by a multiplication factor as some were stored as 
                ### 'kg m-2 s-1' i.e. mm/s
                # breakpoint()
                try:
                    print(da.units)
                except:
                    print(f'No units found in {cpm}')
                    breakpoint()
                multiply = 1
                if ( da.units == 'kg m-2 s-1'):
                    print('Units in mm/s, mean: ' + str(da.values.mean()))
                    if time_res == '1hr':
                        hd = 'mm/h' 
                    elif time_res == '1d':
                        hd = 'mm/d'
                    print(f'Adjusted to {hd}, mean: ' + str(da.values.mean()*3600))
                    # da = da * 3600
                    multiply = 3600
                elif (da.values.mean() > 0.001 and da.units == 'kg m-2 s-1'):
                    print(f'Check units of {cpm}, maybe values were adjusted' +
                           ' wrongly (multiplied by 3600)')
                    print(f'Current units: {da.units}, \n Current mean values:'
                          +f' {da.values.mean()}, \n will be multyplied by 3600')
                elif (da.values.mean() < 0.001 and da.units != 'kg m-2 s-1'):
                    print(f'Check units of {cpm}')

                # if (scenario == 'evaluation' and cpm == 'CLMcom-ETH' and 
                #     time_res == '1d'):
                #     multiply = 1
                #     print(f'Unitsnits: {da.units}, \n Current mean values:'
                #           +f' {da.values.mean()}')
                    
                # try:
                #     if (da.units != 'kg m-2 s-1' and
                #          time_res in ['1hr','3hr','6hr']): 
                #         print(f'Error in {cpm} - make sure to check its units')
                #         raise RuntimeError
                #     if da.units != 'mm/d' and time_res in ['1d','3d','5d']: 
                #         if da.units == 'mm/h' and cpm == 'MOHC': 
                #             pass
                #         else:
                #             da.attrs['units'] = 'mm/d'
                            
                # except:
                #     print(f'Error with {cpm} - make sure to check its units')
                #     pass
                
                if time_res in ['1hr','3hr','6hr']:
                    # multiply = 3600
                    if time_res == '1hr':
                        maxima_threshold = 250
                    elif time_res == '3hr':
                        maxima_threshold = 350
                    elif time_res == '6hr':
                        maxima_threshold = 450
                elif time_res in ['1d','3d','5d']:
                    # if da.values.mean() < 0.001 and da.values.mean() > 0:
                    #     multiply = 3600
                    # else:
                        # multiply = 1
                    if time_res == '1d':
                        maxima_threshold = 1000
                    elif time_res == '3d':
                        maxima_threshold = 1300
                    elif time_res == '5d':
                        maxima_threshold = 1500
                    # if cpm in ['CLMcom-ETH', 'CNRM', 'ICTP', 'CLMcom-CMCC'] and rcm == None:
                    #     multiply = 3600
                    # elif (cpm == 'CLMcom-CMCC' and scenario == 'evaluation'
                    #        and time_res == '1d' and rcm == None):
                    #     multiply = 1
                    # elif (cpm == 'CLMcom-ETH' and rcm == 1 and time_res == '1d'):
                    #     multiply = 3600
                if (time_res == '1d' and scenario == 'evaluation' and
                     cpm == 'CLMcom-CMCC') and region != 'CH':
                    ds = ds.rename({'x':'rlon','y':'rlat'})
                    da = da.rename({'x':'rlon','y':'rlat'})

                if cpm == 'CLMcom-CMCC' and time_res != '1d': 
                    da_masked = masking(da, mask_region)

                else:
                    # breakpoint()
                    t_beg = time.time()
                    xrda_masked = da.where(mask_region == i_region,
                                            drop = True)
                    da_masked = np.nan_to_num(xrda_masked.values) * multiply
                    print('Time for masking and changing units: ' + 
                            '%.2f' % (time.time() - t_beg) + ' s')

                ### Remove timesteps that include values that are unreasonable
                ### i.e higher than a set threshold
                ind_remove = np.where(da_masked >= maxima_threshold)

                if np.any(ind_remove[0]):
                    ind_remove = np.sort(np.array(list(set(ind_remove[0]))))
                    for i_rem in ind_remove:
                        try:
                            np.concatenate((da_masked[:i_rem,:,:],
                                        da_masked[i_rem+1:,:,:]), axis = 0)
                        except:
                            pass
                    print(f'Removed {len(ind_remove)} timesteps with ' 
                          +f'unreasonable values. | {cpm}, {i_year}, {season}')
                
                # ### Set individual gridcell that are unreasonable to nan
                # da_masked = np.where(da_masked >= maxima_threshold, np.nan,
                #                       da_masked)  
               
                  
                # '''-------------------------------------------------------'''
                ### Quick plot to check if region masking worked
                # '''-------------------------------------------------------'''

                # lat = da.lat.where(mask_region[0] == i_region, drop = True)
                # lon = da.lon.where(mask_region[0] == i_region, drop = True)
                # # map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
                # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
                # rad_earth = 6371.0  # approximate radius of Earth [km]
                # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
                #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
                # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

                # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                #                     subplot_kw={'projection':ccrs.PlateCarree()})
                # ax.coastlines(color='grey', linewidth=0.5)
                # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
                # ax.set_aspect("auto")
                # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
                # ax.contourf(lon, lat, xrda_masked[0], 
                #     cmap='terrain', transform= ccrs.PlateCarree())
                # plt.savefig(os.path.join(plot_path, 'masktest_xarray.pdf'),
                #                         format='pdf',bbox_inches='tight')
            
                # '''-----------------------------------------------------------'''
                ### Calculate maxima of each gridcell of one year. Ensure 
                ### independence by removing 2 days worth of timesteps before and
                ### after maxima
                # '''-----------------------------------------------------------'''
                maxima_num = 1

                def independent_maxima(array):
                    '''
                    Returns maxima of time axis of input 3D-array and changes input
                    array to ensure indpendence for next maxima (removes maxima
                    that were found + two days before and after)

                    ''' 
                    t_beg = time.time()

                    if time_res == '1hr':
                        twoday = 48
                    elif time_res == '3hr':
                        twoday = 16
                    elif time_res == '6hr':
                        twoday = 8
                    elif time_res == '1d':
                        twoday = 2
                    elif time_res == '3d':
                        twoday = 1
                    elif time_res == '5d':
                        twoday = 1
                    
                    try:
                        ind_array = np.nanargmax(array, axis = 0, keepdims = True)
                    except ValueError:
                        breakpoint()
                    ### Find maxima and save values
                    max_array = np.nanmax(array, axis = 0, keepdims = True)
                    global maxima_ten_year
                    maxima_ten_year.append(max_array)
                    
                    ### Exclude maxima and values two days before and after max value
                    for i_erase in range(twoday):

                        try:
                            np.put_along_axis(array, ind_array + i_erase, 
                                            0.0, axis = 0)
                        except IndexError:
                            continue

                        try: 
                            np.put_along_axis(array,ind_array - i_erase,
                                            0.0, axis = 0)
                        except IndexError:
                            continue
                            
                    global maxima_num
                    print(f'Time for calculating maxima {maxima_num}: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')
                    maxima_num+=1

                    return array, max_array

                ### For 'DJF' in the first year only consider January and February and
                ### only take 2 maxima. The last Year only use December and only one 
                ### maxima.
                if season == 'DJF' and i_year == 0:
                    ### Get two maxima of season (January, February)
                    array_loop1, maxima_1 = independent_maxima(da_masked)
                    array_loop2, maxima_2 = independent_maxima(array_loop1)
                elif season == 'DJF' and i_year == (range_num-1):
                    ### Get one maxima of season (December)
                    array_loop1, maxima_1 = independent_maxima(da_masked)
                else:
                    ### Get three maxima of season
                    array_loop1, maxima_1 = independent_maxima(da_masked)
                    array_loop2, maxima_2 = independent_maxima(array_loop1)
                    array_loop3, maxima_3 = independent_maxima(array_loop2)
                da.close()
                mask_region.close()
                # breakpoint()
            # '''-----------------------------------------------------------'''
            ### Masking dry areas (>15 maxima with <1mm/h)
            # '''-----------------------------------------------------------'''
            maxima_ten_year = np.squeeze(np.array(maxima_ten_year))
            dry_areas = np.count_nonzero(maxima_ten_year < 1, axis = 0)
            maxima_no_dry = np.where(dry_areas > 15, np.nan, maxima_ten_year)

            # '''-----------------------------------------------------------'''
            # ## Quick plot to check if masking dry areas worked
            # '''-----------------------------------------------------------'''

            # # map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
            # # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
            # # rad_earth = 6371.0  # approximate radius of Earth [km]
            # # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            # #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            # # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

            # # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
            # #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # fig, ax = plt.subplots(figsize=(12.0 , 12.0),
            #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # ax.coastlines(color='grey', linewidth=0.5)
            # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
            # ax.set_aspect("auto")
            # # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.contourf(xrda_masked.rlon, xrda_masked.rlat, maxima_no_dry[0], 
            #     cmap='terrain', transform= ccrs.PlateCarree())
            # plt.show()
            # breakpoint()
            # plt.savefig(os.path.join(plot_path, 'maxima_np_dry.pdf'),
            #                         format='pdf',bbox_inches='tight')
            
            # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
            #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # ax.coastlines(color='grey', linewidth=0.5)
            # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
            # ax.set_aspect("auto")
            # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.contourf(mask_region.lon, mask_region.lat, maxima_ten_year[0], 
            #     cmap='terrain', transform= ccrs.PlateCarree())
            # plt.savefig(os.path.join(plot_path, 'maxima_all.pdf'),
            #                         format='pdf',bbox_inches='tight')

            ### For DJF first two values are maxima of first year (Jan, Feb) and last 
            ### value is maxima of last year (Dec). Otherwise and for all other seasons
            ### three values == first, second and third maxima of that year/season
            try:
                rm_old_file = ('rm ' + path_save + 
                               f'/maxima_{cpm}_{region}_{season}.npy')
                os.system(rm_old_file)
            except:
                print('No old file to remove')
            np.save(path_save + '/' + f'maxima_{cpm}_{region}_{season}.npy',
                    np.array(maxima_no_dry, dtype=object), allow_pickle=True)
            
            ds.close()
            
