#!/bin/bash -l
#SBATCH --job-name="YourJobName"        # CHANGE THIS 
#SBATCH --account="YourAccount"         # CHANGE THIS 
#SBATCH --mail-type=ALL
#SBATCH --mail-user=YourMail@mail.com   # CHANGE THIS 
#SBATCH --time 24:00:00                 # CHANGE THIS 
#SBATCH --nodes=1
#SBATCH --error=filename.err
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=normal
#SBATCH --constraint=gpu
#SBATCH --hint=nomultithread
#SBATCH --exclusive


export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export CRAY_CUDA_MPS=1

module load daint-gpu
module load CDO

# CHANGE THIS DEPENDING ON WHICH ENVIRONMENT YOU NEED
# conda activate alps
# conda activate ch2025

# RUN WITH ALPS ENVIRONMENT (UNCOMMENT WHICHEVER YOU WANT TO RUN)
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/pr_aggregation.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/seasons_calc_maxima.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/Save_as_ncfile.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/temp_change.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/calc_ensemble_ncfile.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/calc_ensemble_scaling.py


# RUN WITH CH2025 ENVIRONMENT (UNCOMMENT WHICHEVER YOU WANT TO RUN)
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/bootstrapping.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/rearange_reduce.py
# srun python /users/lgimmi/MeteoSwiss/ch2025-prototype/stations_maxima.py
