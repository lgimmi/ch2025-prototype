#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Plot different Bootstrap numbers against each other to find optimal

"""
import os
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches

'''-------------------------------------------------------------------------'''
### evaluation, historical
scenario = 'evaluation'

time_res = '1hr'

# number_of_bootstraps = 50
which_bootstrap = np.arange(5,305,5)

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)

path_data = (f'/scratch/snx3000/lgimmi/store/miscellaneous/station_bootstrap')
path_save_fig = (f'/users/lgimmi/MeteoSwiss/figures/allgemein')

'''-------------------------------------------------------------------------'''
### only for Summer so far
fig = plt.figure(figsize=(18, 6))
gs = fig.add_gridspec(4, 2)
ax1 = fig.add_subplot(gs[0:3, :])
ax2 = fig.add_subplot(gs[3, :])

color_1=cm.rainbow(np.linspace(0,1,len(return_periods)))
color_2=iter(cm.rainbow(np.linspace(0,1,len(return_periods))))
cmap_boot = LinearSegmentedColormap.from_list('Bootstrap', color_1,
                                              N=len(return_periods))
rel_dict = {}
for num_btstrap in which_bootstrap:
    data = np.load(path_data + 
    f'/Delta_Spread_{num_btstrap}_{time_res}.npy', allow_pickle=True)
    rel_data = np.load(path_data + 
    f'/Relative_Spread_{num_btstrap}_{time_res}.npy', allow_pickle=True)

    for rp in range(len(return_periods)):
        if num_btstrap == which_bootstrap[0]:
            rel_dict[return_periods[rp]] = [rel_data[rp]]
        else:
            rel_dict[return_periods[rp]] += [rel_data[rp]]

    x = np.full(len(data),num_btstrap)
    col = ax1.scatter( x, data, c=return_periods, cmap=cmap_boot)

    if num_btstrap == 50:
        ### Add black line
        ax2.plot( [num_btstrap,num_btstrap], [0,rel_data[rp]],'-', c='k',
                 zorder=100)

for rp in range(len(return_periods)):
    col_plot = next(color_2)
    ax2.plot(which_bootstrap, rel_dict[return_periods[rp]],'-',
                color=col_plot)
    
# cbar_ax= fig.add_axes([0.93,0.33,0.02,.45])
cbar_ax= fig.add_axes([0.92,0.122,0.02,.75])
cbar = plt.colorbar(col, ax=ax1, cax=cbar_ax, label="Returnperiod",
                    orientation="vertical", ticks=return_periods)
cbar.set_ticklabels(return_periods)
cbar.ax.tick_params(labelsize=8)

ax2.set_xlabel("# of Bootstraps", fontsize=12)
ax1.set_ylabel(" Absolute Difference", fontsize=10)
ax2.set_ylabel(" Relative Difference", fontsize=10)
plt.xlabel('Returnperiods', fontsize=8)
plt.suptitle(f'Station data uncertainty (90P - Median)for different # of Bootstraps ',
                      y=0.93, fontsize=15)

ax1.set_xlim(0,which_bootstrap[-1]+5)
ax2.set_xlim(0,which_bootstrap[-1]+5)
ax2.set_ylim(0,1)
ax1.set_xticks([])

plt.savefig(os.path.join(path_save_fig, f'Bootstrap_test.pdf'),
                    format='pdf',bbox_inches='tight')
print('Done')