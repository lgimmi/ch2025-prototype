#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Map multimodel mean for every returnperiod

"""
import os
import copy
import xesmf as xe
import numpy as np
import xarray as xr 
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature

'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'evaluation'

### xHourly, xDaily
time_res = '1d'

### Choose stat from ['Mean','Median','10P','90P'] as indexnumber
### Stabndard is Median
stat = 1
### uncertauinty is shown as the fraction of the 90-quartile and the median

### Define Returnperiods for which values GEV values should be calculated
return_periods = [2,5,10,20,30,50,100,200,300]
# return_periods = np.arange(5,105,5)
# return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])
# rp_list = np.arange(2,201,1).tolist()

seasons = ['JJA', 'DJF', 'SON', 'MAM']
# seasons = ['JJA']

### Use combination of regions
regions = {"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}

path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}') 

# Institutes = ['EnsembleMean','CLMcom-BTU','CLMcom-ETH','CLMcom-JLU',
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']

Institutes = ['CLMcom-BTU','CLMcom-ETH','CLMcom-JLU','CLMcom-CMCC',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','EnsembleMean',
              'MOHC','ICTP']
if scenario == 'evaluation' and time_res == '1d':
    Institutes = ['CLMcom-BTU','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','EnsembleMean',
              'MOHC','ICTP']

modelagreement = []
modelagreement_neg = []
for i_cpm, cpm in enumerate(Institutes):
    if cpm != 'EnsembleMean':
        modelagreement.append([])
        modelagreement_neg.append([])
        if i_cpm != 0:
            modelagreement[i_cpm-1] = np.asarray(modelagreement[i_cpm-1])
            modelagreement_neg[i_cpm-1] = np.asarray(modelagreement_neg[i_cpm-1])
    if time_res == '1hr':
        plot_path = (f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/maps/'
                     +f'{cpm}/{scenario}/')
    elif time_res == '1d':
        plot_path = (f'/users/lgimmi/MeteoSwiss/figures/day/maps/'
                    +f'{cpm}/{scenario}/')

    if scenario == 'rcp85_moc':
        path_data = path_data[:-4] + '/moc'
        plot_path = plot_path[:-11] + '/moc'
    elif scenario == 'rcp85_eoc':
        path_data = path_data[:-4] + '/eoc'
        plot_path = plot_path[:-11] + '/eoc'
    '''---------------------------------------------------------------------'''
    ### Use same levels as in https://www.meteoschweiz.admin.ch/service-und-publikationen/applikationen/ext/climate-extremes-maps.html
    ### so that it is comparable.
    if time_res == '1hr':
        levels_dict = {
            2: [0,4,5,6,7,8,10,12,14,16,20,24,28,34,40],
            5: [0,5,6,7,9,11,13,15,18,22,27,32,39,47,56],
            10: [0,6,10,14,18,22,26,30,34,38,42,46,50,54,64],
            20: [0,7,8,10,12,15,18,22,26,32,38,46,56,68,82],
            30: [0,7,13,19,25,31,37,43,49,55,61,67,73,79,85],
            50: [0,8,10,12,14,17,21,26,31,38,46,56,68,83,101],
            100: [0,9,11,13,16,20,24,29,36,44,53,65,79,96,117],
            200: [0,9,11,14,17,21,25,31,38,47,58,71,88,108,133],
            300: [0,10,12,15,19,23,28,34,42,52,63,78,96,117,144]
        }
    elif time_res == '1d':
         levels_dict = {
             2: [0,50,55,60,65,70,80,90,100,110,120,130,150,170,190],
             5: [0,60,65,70,75,80,90,100,110,120,140,160,180,200,230],
             10: [0,70,75,80,90,100,110,120,130,140,160,180,210,240,280],
             20: [0,75,80,90,100,110,120,130,140,160,180,210,240,280,320],
             30: [0,80,90,100,110,120,130,140,160,180,210,240,270,300,350],
             50: [0,90,100,110,120,130,140,150,170,190,220,260,300,340,380],
             100: [0,100,110,120,130,140,150,160,180,200,230,260,300,350,420],
             200: [0,110,120,130,140,150,160,180,200,230,260,300,350,400,460],
             300: [0,120,130,140,150,160,180,200,230,260,300,350,400,450,510]
         }
    levels_uncertainty = [0,0.15,0.2,0.3,0.5,0.7,1,1.5,2]
   

    def fill_patches_with_NNmean(arr, patch_size=5):
        '''
        Fixes patches created by regridding of models
        '''
        from scipy.interpolate import griddata

        filled_arr = np.copy(arr)

        # Find nan to values transitions which are not at the border, i.e.
        # patches to be filled
        patches = np.argwhere(np.isnan(arr) & 
                                ~np.isnan(np.roll(arr,patch_size)) &
                                ~np.isnan(np.roll(arr,-patch_size)))
        
        # Interpolate using griddata where we have patches
        filled_arr[tuple(zip(*patches))] = \
                griddata(np.array(np.where(~np.isnan(arr))).T,
                        arr[~np.isnan(arr)],
                        tuple(patches.T),
                        method='nearest')
        
        return filled_arr


    for i_season, season in enumerate(seasons):
        print(f'{i_season+1}/{len(seasons)}')
        if cpm != 'EnsembleMean':
            modelagreement[i_cpm].append([])
            modelagreement_neg[i_cpm].append([])
            filename = (path_data + 
                        # f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                    f'/{cpm}_{time_res}_RP_prmax_{season}.nc')
        else:
            filename = path_data + f'/{cpm}_{time_res}_RP_prmax_{season}.nc'
        ds_region = xr.open_mfdataset(filename)

        # '''-----------------------------------------------------------'''
        ###  Plotting combined regions
        # '''-----------------------------------------------------------'''
        ### MMM is in rotated lat lon grid, get information
        if cpm not in ['CNRM','KNMI','HCLIMcom','ICTP','MOHC']:
            rlat,rlon,pole = ds_region.rlat, ds_region.rlon, ds_region.rotated_pole
            pole_lon = pole.attrs['grid_north_pole_longitude']
            pole_lat = pole.attrs['grid_north_pole_latitude']

            crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

        elif cpm == 'MOHC':
            rlat,rlon,pole = ds_region.rlat, ds_region.rlon, ds_region.rotated_latitude_longitude
            pole_lon = pole.attrs['grid_north_pole_longitude']
            pole_lat = pole.attrs['grid_north_pole_latitude']

            crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

        else:
            rlat,rlon = ds_region.lat, ds_region.lon

        ### Set map extent
        # map_ext = np.array([-180, 180, -90, 90])  # [degree]
        # map_ext = np.array([-0, 11, 45, 49])  # [degree]
        map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
        rad_earth = 6371.0  # approximate radius of Earth [km]
        dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
        dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

        for i_rp, rp in enumerate(return_periods):
            i_rp_ma = copy.deepcopy(i_rp)
            # i_rp = rp_list.index(rp)
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ### Absolute values
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            ### Create figure that is adjusted for latitudinal distorion
            fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                                subplot_kw={'projection':ccrs.PlateCarree()})

            ax.coastlines(color='black', linewidth=0.5)
            ax.add_feature(cfeature.BORDERS, edgecolor= 'black',linewidth = 0.5)
            ax.set_aspect("auto")
            ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            color_list = [  '#feb24c', '#fed976', '#ffeda0',
                            '#edfac2', '#cdffcd', '#99f0b2',
                            '#53bd9f', '#32a696', '#3296b4',
                            '#0570b0', '#05508c', '#0a1f96',
                            '#2c0246', '#6a2c5a', '#a8415b']

            cmap = mpl.colors.ListedColormap(color_list)
            # norm = mpl.colors.BoundaryNorm(boundaries=levels_dict[rp], 
            #                                ncolors=len(cmap.colors))
            norm = mpl.colors.BoundaryNorm(levels_dict[rp], len(cmap.colors)-1)

            for p_i, p_reg in enumerate(regions.keys()):
                existing_rp = ds_region[p_reg]['returnperiod'].values.tolist()
                i_rp = existing_rp.index(rp)
                if p_i == 0:
                    try:
                        mymap = ds_region[p_reg][i_rp][stat].values
                    except:
                        breakpoint()
                    uncertainty = (ds_region[p_reg][i_rp][1].values / 
                                ds_region[p_reg][i_rp][3].values)
                else:
                    mymap = np.where(np.isnan(mymap) == True,
                                    ds_region[p_reg][i_rp][stat].values, mymap)
                    relation = (ds_region[p_reg][i_rp][1].values / 
                                ds_region[p_reg][i_rp][3].values)
                    uncertainty = np.where(np.isnan(uncertainty) == True,
                                    relation, uncertainty)
            
            mymap = fill_patches_with_NNmean(mymap)
            uncertainty = fill_patches_with_NNmean(uncertainty,patch_size=7)

            if cpm != 'EnsembleMean':
                ### Create new mask of switzerland for modelagreement
                positives = np.where(np.isnan(mymap) == True, np.nan, 0)
                negatives = np.where(np.isnan(mymap) == True, np.nan, 0)

                ### Append Bool mask where values are positive
                try:
                    modelagreement[i_cpm][i_season].append(mymap > 0)
                    modelagreement_neg[i_cpm][i_season].append(mymap < 0)
                except:
                    breakpoint()
                
            else:
                for i_Inst in range(len(Institutes)-1):
                    if i_Inst == 0:
                        positives = (modelagreement[i_Inst][i_season][i_rp_ma]*1)
                        negatives = (modelagreement_neg[i_Inst][i_season][i_rp_ma]*1)
                    else:
                        positives += (modelagreement[i_Inst][i_season][i_rp_ma]*1)
                        negatives += (modelagreement_neg[i_Inst][i_season][i_rp_ma]*1)


            if cpm not in ['CNRM','KNMI','HCLIMcom','ICTP']:
                col = ax.pcolormesh(rlon, rlat, mymap, cmap=cmap, norm=norm,
                            transform=crs, rasterized=True)
            else:
                col = ax.pcolormesh(rlon, rlat, mymap, cmap=cmap, norm=norm,
                                    rasterized=True)
            
            if cpm == 'EnsembleMean':
                limit = len(Institutes) - 4
                hatch = np.where(positives < limit , 1, np.nan) 
                hatch_neg = np.where(negatives < limit, 1, np.nan)
                mpl.rcParams['hatch.linewidth'] = 0.5
                main_hatch = hatch * hatch_neg
                new_main_hatch = np.where(np.isnan(mymap) == True, 
                                       np.nan, main_hatch)
                ax.pcolor(rlon, rlat, new_main_hatch, hatch='//', alpha=0.,
                          edgecolor='k', linewidth=0.1)
                
            ### Add text box with regional mean
            regional_mean = np.nanmean(mymap)
            props = dict(boxstyle='Square', facecolor='white', alpha=0.5)
            ax.text(0.02,0.95,f'Regional mean: {regional_mean:.2f}%',
                    transform=ax.transAxes, bbox=props, fontsize=16)
            
            if cpm == 'EnsembleMean':
                ### Add explanation of hatching
                props2 = dict(boxstyle='Square', facecolor='white',
                            edgecolor='white', alpha=0.)
                ax.text(0.77,0.08,f'Color', transform=ax.transAxes, bbox=props,
                        fontsize=10)
                ax.text(0.82,0.08,f'High Model Agreement',
                         transform=ax.transAxes, bbox=props2, fontsize=10)
                ax.text(0.77,0.04,f'  ///// ', transform=ax.transAxes,
                         bbox=props, fontsize=10)
                ax.text(0.82,0.04,f'Low model agreement',
                         transform=ax.transAxes, bbox=props2, fontsize=10)
                
            ### remove frame of figure
            fig.patch.set_visible(False)
            ax.axis('off') 

            # cbar = plt.colorbar(col, ticks=levels_dict[rp], format='%i')
            cbar_ax= fig.add_axes([0.92,0.122,0.03,.75])
            cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap,
                                    norm=norm,
                                    boundaries=levels_dict[rp],
                                    extend='max',
                                    ticks=levels_dict[rp],
                                    #spacing='proportional',
                                    orientation='vertical',
                                    label='[mm]')
            
            # cbar = plt.colorbar(col, cax=cbar_ax,
            #                     ticks=levels_dict[rp], extend='max',
            #                     label="Returnvalue [mm/h]", 
            #                     orientation="vertical")
            # cbar.set_ticklabels(return_periods)
            cbar.ax.tick_params(labelsize=10)
            if scenario in ['evaluation', 'historical']:
                scen_name = scenario.capitalize()
            else:
                scen_name = scenario[-3:].capitalize()
            if time_res == '1hr':
                title_name = '60-minute'
            elif time_res == '1d':
                title_name = 'One day'

            if cpm == 'EnsembleMean':
                plt.suptitle((f'{title_name} precip.: {rp}-year return level (mm), '
                            +f'{season_names[season]} | {scen_name} |'
                            +' Ensemble Mean'),
                            y=0.93, fontsize=17)
            else:
                plt.suptitle((f'{title_name} precip.: {rp}-year return level (mm), '
                            +f'{season_names[season]} | {scen_name} |'
                            +f' {cpm}'),
                            y=0.93, fontsize=17)
            # plt.show()
            # breakpoint()

            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ### Add Station smarties
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            if scenario in ['evaluation', 'historical']:
                ### Get station data
                path_stations = '/users/lgimmi/MeteoSwiss/data' 
                path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
                filename_station = (path_data + f'/Values_stations_{season}_'
                                +f'{scenario}_{time_res}.npy')
                filename_region = (path_region_stations + 
                                f'/stations_region_{scenario}_{time_res}.csv')
                station_data = np.load(filename_station, allow_pickle=True)
                station_region = pd.read_csv(filename_region)
                st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []

                # st1, st2, st3, st4 = [],[],[],[]
                # for i_station, station in enumerate(station_region.keys()):
                #     st4.append(station_data[i_station][rp][1])

                ### Get station location
                if time_res == '1hr':
                    filename_stations = \
                        (path_stations + '/station_metadat_rre150h0_1995_2010_'
                        +'allstats_andpartnerstats_withNA.csv')
                elif time_res == '1d':
                    filename_stations = \
                        (path_stations + '/station_metadat_rre150d0_2000_'
                        +'2009_allstatswithpartnerstats_allNA.csv')         

                ### Regridd station data (only once thus green)
                try:
                    filename = (path_data + 
                                f'/CLMcom-ETH_{time_res}_RP_prmax_{season}.nc')
                    target_grid = xr.open_dataset(filename)
                    fname_region = (path_region_stations +
                                     '/station_region_latlon.nc')
                    region_grid = xr.open_dataset(fname_region)
                    regridder = xe.Regridder(region_grid, target_grid, "bilinear")
                    stations_regions = regridder(region_grid)
                    filename_new = (path_region_stations + 
                                    '/Regridded_station_region_latlon.nc')
                    stations_regions.to_netcdf(path=filename_new)

                    fname_region = path_region_stations + '/station_region_latlon_maskCH.nc'
                    region_grid = xr.open_dataset(fname_region)
                    regridder = xe.Regridder(region_grid, target_grid, "bilinear")
                    ds_regridded = regridder(region_grid)
                    filename_new = (path_region_stations + 
                                    '/Regridded_station_region_latlon_maskCH.nc')
                    ds_regridded.to_netcdf(path=filename_new)
                except:
                    pass
                
                filename_regions = (path_region_stations + 
                                '/Regridded_station_region_latlon.nc')
                stations_regions = xr.open_dataset(filename_regions)
                
                ch = stations_regions['orog']
                data = pd.read_csv(filename_stations)
                lon = data['longitude']
                lat = data['latitude']

                num_of_stations = 0
                
                for i, station in enumerate(data['nat_abbr']):

                    if (scenario == 'evaluation' and station in 
                            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                            'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                            'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                            'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                            'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                            'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                            'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                            'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                            'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                            'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                            'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                            'ZHWIN','ZHZEL'
                            ]):
                            continue
                    if (scenario == 'historical' and station in 
                            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                            'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                            'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                            'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                            'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                            'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                            'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                            'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                            'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                            'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                            'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                            'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                            'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                            'TIBED','TIOLI'
                            ]):
                            continue

                    check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
                    check_lat = round(lat[i]/2,2)*2 ### lon in file are even
                    station_names = station_region.keys().values.tolist()
                    if station in station_names:
                        # i_station = station_names.index(station)
                        smarties_value = station_data[station][rp][1]
                        closest_value = \
                            min(levels_dict[rp], key=lambda x: abs(smarties_value - x))
                        closest_index = levels_dict[rp].index(closest_value)
                        smarties_color = color_list[closest_index-1]

                        ax.scatter(check_lon, check_lat, data=smarties_value, 
                                c=smarties_color, s=50,zorder=num_of_stations) 
                        ax.plot(check_lon, check_lat, 'ko', markersize=8,
                                fillstyle='none',zorder=num_of_stations) 

                        num_of_stations += 1

                xend = 11.0
                ybottom = 45.08

                ### Add circle legend
                ax.text(xend, ybottom, '◯ Stations', fontsize=10, weight='bold',
                            verticalalignment='center', horizontalalignment='center')
        
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            if cpm == 'EnsembleMean':
                plt.savefig(os.path.join(plot_path, f'Ensemble_{season}_RP{rp}.pdf'),
                            format='pdf',bbox_inches='tight')
            else:
                plt.savefig(os.path.join(plot_path, f'{season}_RP{rp}.pdf'),
                            format='pdf',bbox_inches='tight')
            plt.close()


            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ### Uncertainty Plot
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            fig2, ax2 = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                                subplot_kw={'projection':ccrs.PlateCarree()})
            
            ax2.coastlines(color='black', linewidth=0.5)
            ax2.add_feature(cfeature.BORDERS, edgecolor= 'black',linewidth = 0.5)
            ax2.set_aspect("auto")
            ax2.set_extent(map_ext, crs=ccrs.PlateCarree())

            cmap2 = mpl.colors.ListedColormap([ '#ffffe5', '#fff7bc', '#fee391',
                                                '#fec44f', '#fe9929', '#ec7014',
                                                '#cc4c02', '#993404', '#662506' ])
            norm2 = mpl.colors.BoundaryNorm(levels_uncertainty,len(cmap2.colors)-1)
            
            uncertainty = fill_patches_with_NNmean(uncertainty)

            if cpm not in ['CNRM','KNMI','HCLIMcom','ICTP']:
                col = ax2.pcolormesh(rlon, rlat, uncertainty, cmap=cmap2,
                                   norm=norm2, transform=crs,rasterized=True)
            else:
                col = ax2.pcolormesh(rlon, rlat, uncertainty, cmap=cmap2,
                                    norm=norm2, rasterized=True)
            cbar_ax2 = fig2.add_axes([0.92,0.122,0.03,.75])
            cbar2 = mpl.colorbar.ColorbarBase(cbar_ax2, cmap=cmap2,
                                    norm=norm2,
                                    boundaries=levels_uncertainty,
                                    extend='max',
                                    ticks=levels_uncertainty,
                                    #spacing='proportional',
                                    orientation='vertical')
            cbar2.ax.tick_params(labelsize=10)
            if scenario in ['evaluation', 'historical']:
                scen_name = scenario.capitalize()
            else:
                scen_name = scenario[-3:].capitalize()
            if cpm == 'EnsembleMean':
                plt.suptitle((f'60-minute precip.: rel.uncert. of {rp}-year return '
                            +'level (fraction), '
                            +f'{season_names[season]} | {scen_name} |'
                            +' Ensemble Mean'),
                            y=0.93, fontsize=17)
                plt.savefig(os.path.join(plot_path, 
                                    f'Uncertainty_{season}_RP{rp}.pdf'),
                                    format='pdf',bbox_inches='tight')
            else:
                plt.suptitle((f'60-minute precip.: rel.uncert. of {rp}-year return '
                        +'level (fraction), '
                        +f'{season_names[season]} | {scen_name} |'
                        +' {cpm}'),
                        y=0.93, fontsize=17)
                plt.savefig(os.path.join(plot_path, 
                                    f'Uncertainty_{season}_RP{rp}.pdf'),
                                    format='pdf',bbox_inches='tight')
        
            # plt.show()
            # breakpoint()
            plt.close()
