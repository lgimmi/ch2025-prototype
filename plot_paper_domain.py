 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
April 2024

Authors:
- Leandro Gimmi

Description:
Plot analysis domain (Topography) of Alp-3 and Switzerland with Stations
"""
import os
import string
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical 
scenario = 'historical'

### xHourly, xDaily
time_res = '1d'

region = 'CH'

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}

topo_file = f'/scratch/snx3000/lgimmi/store/ALP-3/orog/Regridded_*{scenario}*'
topo_file_og = f'/scratch/snx3000/lgimmi/store/ALP-3/orog/orog*{scenario}*'

### --------------------------------------------------------------------###
### Add overview of region and stations to plot
### --------------------------------------------------------------------###

# map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
map_ext = np.array([5.7, 10.9, 45.7, 47.9])  # [degree]
rad_earth = 6371.0  # approximate radius of Earth [km]
dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(map_ext[2:].mean()))) \
         / 360.0 * (map_ext[1] - map_ext[0])
dist_y = 2.0 * np.pi * rad_earth / 360.0 * (map_ext[3] - map_ext[2])
fig = plt.figure(figsize=(11, 11 * (dist_y / dist_x)))
gs = fig.add_gridspec(nrows=1, ncols=1)
ax = fig.add_subplot(gs[0,0], projection=ccrs.PlateCarree())

ax.coastlines(color='grey', linewidth=0.5)
ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
    linewidth = 0.5 )
ax.set_aspect("auto")
ax.set_extent(map_ext, crs=ccrs.PlateCarree())


### open region and station data
plot_path = f'/users/lgimmi/MeteoSwiss/figures/paper/domain/'
path_stations = '/users/lgimmi/MeteoSwiss/data' 
path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
if time_res == '1hr':
    filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                        +'allstats_andpartnerstats_withNA.csv')
elif time_res == '1d':
    filename_stations = (path_stations + '/station_metadat_rre150d0_2000_'
                            +'2009_allstatswithpartnerstats_allNA.csv')

if region != 'CH':
    ### Extended Switzerland
    filename_regions = (path_region_stations + '/station_region_latlon.nc')
else:
    filename_regions = (path_region_stations + 
                        '/station_region_latlon_maskCH.nc')

regions = xr.open_dataset(filename_regions)
topo_nc = xr.open_mfdataset(topo_file)
topo = topo_nc.orog

if region != 'CH':
    ### Extended Switzerland
    ch = regions.orog
else:
    ch = regions['mask_CH']

data = pd.read_csv(filename_stations)
lon = data['longitude']
lat = data['latitude']

colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
        "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}
# colors = {"CH":"#C31616", "CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
#         "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

# cmap = LinearSegmentedColormap.from_list('custom_colormap', [colors[region],
#                                         (1, 1, 1)] , N=2 )
terrain_cmap = plt.get_cmap('terrain')
terrain_colors = terrain_cmap(np.linspace(0.25, 1, 256))
terrain_colors[:20, :3] *= 0.9
terrain_colors[20:50, :3] *= 0.95
new_colors = np.insert(terrain_colors, 0, [0.1, 0.1, 0.5, 0.8], axis=0)  # Red color: [R, G, B, alpha]
custom_cmap = LinearSegmentedColormap.from_list('custom_terrain', new_colors, N=256)

masked_region = xr.where(ch == region_numbers[region], topo, np.nan)
station_region = ch.where(ch == region_numbers[region], drop=False)
levels2 = np.linspace(0,3600,19)
levels = np.insert(levels2,0,-6)
if region == 'CH': 
    col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                cmap=custom_cmap, levels=levels2, vmin=0, vmax=4000, alpha=1.0,
                extend='both')
else:
    col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
            cmap=custom_cmap, levels=levels2, vmin=0, vmax=4000, alpha=0.8,
            extend='both')

### Check in which region (rounded) lat/lon of stations are found
num_of_stations = 0
for i, station in enumerate(data['nat_abbr']):

    if (scenario == 'evaluation' and station in 
            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
            'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
            'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
            'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
            'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
            'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
            'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
            'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
            'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
            'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
            'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
            'ZHWIN','ZHZEL'
            ]):
            continue
    # if (scenario == 'historical' and station in 
    #         ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
    #         'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
    #         'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
    #         'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
    #         'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
    #         'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
    #         'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
    #         'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
    #         'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
    #         'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
    #         'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
    #         'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
    #         'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
    #         'TIBED','TIOLI'
    #         ]):
    #         continue
        
    check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
    check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
    if check_lon in masked_region.lon and check_lat in masked_region.lat:
        idx_lon = np.where(masked_region.lon.values == check_lon)[0][0]
        idx_lat = np.where(masked_region.lat.values == check_lat)[0][0]
        if station_region.values[idx_lat][idx_lon]==region_numbers[region]:
            if time_res == '1hr':
                # ax.plot(check_lon, check_lat, 'ko',markersize=2) 
                ax.plot(check_lon, check_lat, 'ko',markersize=1)
            elif time_res == '1d':
                ax.plot(check_lon, check_lat, 'ko',markersize=5)
            num_of_stations += 1

# plt.show()
# breakpoint()
xend = 10.0
ybottom = 46.0
ax.text(xend, ybottom, '● Stations', fontsize=20, weight='bold',
                            verticalalignment='center', horizontalalignment='center')
plt.savefig(os.path.join(plot_path, f'CH_with_Stations.pdf'),
                        format='pdf',bbox_inches='tight')

plt.close(fig)
topo_nc.close
regions.close




### --------------------------------------------------------------------###
### Entire ALPS-3 domain
### --------------------------------------------------------------------###
fig = plt.figure(figsize=(11, 11))
gs = fig.add_gridspec(nrows=1, ncols=1)
ax = fig.add_subplot(gs[0,0], projection=ccrs.PlateCarree())
map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
ax.coastlines(color='black', linewidth=0.5)
ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
    linewidth = 0.5 )
# ax.set_extent(map_ext, crs=ccrs.PlateCarree())
ax.set_aspect("auto")

# ### remove frame of figure
# fig.patch.set_visible(False)
# ax.axis('off') 

topo_nc = xr.open_mfdataset(topo_file_og)
topo = topo_nc.orog

col2 = ax.contourf(topo.lon, topo.lat, topo, cmap=custom_cmap, levels=levels,
                  vmin=0, vmax=4000, alpha=1.0, extend='both')
cbar_ax= fig.add_axes([0.126,0.06,0.75,.03]) # horizontal
# norm = mpl.colors.TwoSlopeNorm(vmin=0, vmax=4000)
# norm = mpl.colors.Normalize(vmin=0, vmax=4000)
# minorticks = np.arange(0, 4100, 100)
# majorticks = np.arange(0, 4500, 500)
cmap = mpl.cm.terrain 

cbar = plt.colorbar( col, cax=cbar_ax, orientation='horizontal', 
                     ticks=np.arange(0,4200,600))
cbar.ax.set_title('m.a.s.l [m]',fontsize=18,y=-2.3)
# cbar = mpl.colorbar.ColorbarBase(cbar_ax, 
#                                  cmap='terrain',
#                                 norm=norm,
#                                 extend='max',
#                                 ticks=majorticks,
#                                 spacing='proportional',
#                                 orientation='horizontal')
cbar.ax.tick_params(labelsize=20)
# cbar.ax.yaxis.set_ticks(minorticks, minor=True)
# cbar.ax.tick_params(size=0,which=u'both') # remove ticks

# plt.show()
plt.savefig(os.path.join(plot_path, f'Alp3.pdf'),
                        format='pdf',bbox_inches='tight')
# breakpoint()