#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
December 2023

Authors:
- Leandro Gimmi

Description:
Aggregate precipitation to different time resolutions

"""

import os
import time
import glob
from natsort import natsorted
import xarray as xr
import warnings
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''
# start_time = time.time()

# in_dir = '/scratch/snx3000/lgimmi/store/ALP-3/1hr/pr/historical/'
# out_dir = '/scratch/snx3000/lgimmi/store/ALP-3/3hr/pr/historical/'

# # Input and output file paths
# input_file = (in_dir + 'pr_ALP-3_MPI-M-MPI-ESM-LR_historical_r1i1p1_CLMcom-ETH-COSMO-crCLIM_fpsconv-x2yn2-v1_1hr_19970101-19971231.nc')

# out_file = (out_dir + 'pr_ALP-3_MPI-M-MPI-ESM-LR_historical_r1i1p1_CLMcom-ETH-COSMO-crCLIM_fpsconv-x2yn2-v1_1hr_19970101-19971231.nc')

# # Open the NetCDF file
# data = xr.open_dataset(input_file)

# # Aggregate precipitation data to 3-hour intervals
# start_time_2 = time.time()
# precipitation_3hr = data['pr'].resample(time='3H').sum()
# end_time_2 = time.time()
# print(f'Aggregation took {end_time_2 - start_time_2} seconds')

# # Save aggregated data to a new NetCDF file
# precipitation_3hr.to_netcdf(out_file)
# end_time = time.time()
# print(f'Whole script took {round(end_time - start_time, 3)} seconds')

in_dir = '/scratch/snx3000/lgimmi/store/rcm/1hr/pr/evaluation/'
out_dir = '/scratch/snx3000/lgimmi/store/rcm/1d/pr/evaluation/'

fnames = (in_dir + '*KNMI*.nc')
# fnames = (in_dir + "pr_WCE-11_KNMI-EC-EARTH_historical_r14i1p1_KNMI-RACMO23E_v1_1hr_20050101-20051231.nc")
filenames_list = natsorted(glob.glob(fnames))
# os_command = ('module load daint ncview')
# os.system(os_command)
os_command = ('module load CDO')
os.system(os_command)

for file_in in filenames_list:
    file_out = file_in.replace('_1hr_', '_1d_')
    file_out = file_out.replace('/1hr/', '/1d/')
    os_command = ('cdo daysum '+ f'{file_in} {file_out}')
    os.system(os_command)

