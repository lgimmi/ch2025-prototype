#!/bin/bash -l
#SBATCH --job-name="eval pr aggregation"
#SBATCH --account="pr133"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=leandrolivio.gimmi@usys.ethz.ch
#SBATCH --time 00:30:00
#SBATCH --nodes=1
#SBATCH --error=agg.err
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=gpu
#SBATCH --hint=nomultithread
#SBATCH --exclusive

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export CRAY_CUDA_MPS=1

module load daint-gpu
module load CDO

### Set time resolution one wants to aggregate to and from which we start
Aggtime_res="1d"
Start_res="1hr"

### Directories and filenames
in_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Start_res/pr/evaluation/"
out_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Aggtime_res/pr/evaluation/"

# in_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Start_res/pr/historical/"
# out_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Aggtime_res/pr/historical/"

# in_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Start_res/pr/rcp85/eoc/"
# out_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Aggtime_res/pr/rcp85/eoc/"

# in_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Start_res/pr/rcp85/moc/"
# out_dir="/scratch/snx3000/lgimmi/store/ALP-3/$Aggtime_res/pr/rcp85/moc/"

input_files=(${in_dir}*.nc)
# input_files=(${in_dir}*ETH*.nc)
# input_files=(${in_dir}*ICTP*.nc)
# input_files=(${in_dir}*CNRM-AROME*.nc)
# input_files=(${in_dir}*CMCC*.nc)

### Temporal aggregation to X-hour/day intervals
task(){
    inname="$(basename $infile)"
    echo $inname
    ### 1-3hr or 1-3day
    # cdo -b F64 -L -timselsum,3  -selvar,pr ${infile} ${out_dir}${inname}
    ### 3-6hr
    # cdo -b F64 -L -timselsum,2  -selvar,pr ${infile} ${out_dir}${inname}
    ### 1-5day
    # cdo -b F64 -L -timselsum,5  -selvar,pr ${infile} ${out_dir}${inname}
    ### 1-6hr
    # cdo -b F64 -L -timselsum,6  -selvar,pr ${infile} ${out_dir}${inname}
    ### 1hr to 1d
    cdo -b F64 -L -timselsum,24  -selvar,pr ${infile} ${out_dir}${inname}
}


### Loop over input files (Parrallel runs)
for infile in ${input_files[@]}
do
    task ${infile} &

done

wait

rename _${Start_res}_ _${Aggtime_res}_ ${out_dir}*.nc
# rename _day_ _${Aggtime_res}_ ${out_dir}*.nc
