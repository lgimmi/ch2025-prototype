#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
September 2023

Authors:
- Leandro Gimmi

Description:
Rerange values from bootstrapping to be easily readable and calculate mean,
median and percentiles for individual return periods (RP)

"""
import os
import copy
import glob
import time
import pickle
import warnings
warnings.simplefilter("ignore")
import numpy as np
import xarray as xr 
import pandas as pd
from multiprocessing import Pool

'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc / rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = '1hr'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
return_periods = np.append(return_periods, [150, 200, 250, 300])

### Should the climate change signal and/or the temperature skaling be 
### calculated 'yes'/'no'. Only works when scenario is rcp85_moc/eoc
### temp_scaling only works if cc_calc also 'yes' as it is needed
cc_calc = 'yes'
temp_scaling = 'yes'
### Breaks the normal data for some reason..

### If the season or the region should be specified please refer to the end of 
### the script and adjust for your liking. Generally all seasons and all 
### CH2018 regions (CHNE, CHW, CHS ,CHAE and CHAW) are used.


### ------------------------------- CPM ----------------------------------- ###
plot_path = '/users/lgimmi/MeteoSwiss/figures'
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')

nc_save_path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
nc_save_data_path_prdata = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}'
                            +'/pr/{scenario}')
nc_save_mask_path_regions = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

# Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
# CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
#         'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
#         'RegCM4-7']
Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
        'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME']

rcm = None

### ------------------------------- RCM ----------------------------------- ###
# plot_path = '/users/lgimmi/MeteoSwiss/figures'
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')

# Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
# CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
#         'RegCM4-7']
# rcm = 1

'''-------------------------------------------------------------------------'''

if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'
if rcm == 1:
    p_name = 'rcm'
else:
    p_name = 'ALP-3'
    
def rearange_reduce(identifier, cpm):
    filename = (path_data + '/' + f'GEV_{cpm}_{region}_{season}.npy')
    data = np.load(filename, allow_pickle=True)
    if scenario in ['rcp85_moc','rcp85_eoc'] and cc_calc == 'yes':
        hist_path = (f'/scratch/snx3000/lgimmi/store/{p_name}/{time_res}/'
                     +'pr_GEV/historical')
        hist_fname = (hist_path + '/' + f'GEV_{cpm}_{region}_{season}.npy')
        hist_data = np.load(hist_fname, allow_pickle=True)

        skaling_path = (f'/scratch/snx3000/lgimmi/store/{p_name}/1d/tas/'
                        +f'cc_signal/{scenario[-3:]}')
        sk_fname = (skaling_path + f'/ccsignal_{cpm}_{region}_{season}.npy')
        sk_data = np.load(sk_fname, allow_pickle=True)
   
    rp_check = 0
    rp_list = []
    rp_values = {}
    h_rp_values = {}
    only_one_time = 0
    t_beg = time.time()

    ### Create empty array to be filled with values
    empty_array = np.full((4,data.shape[0],data.shape[1]),np.nan).tolist()
    for key in return_periods:
        rp_values[key] = copy.deepcopy(empty_array)
        if (scenario in ['rcp85_moc', 'rcp85_eoc'] and cc_calc == 'yes'):
            h_empty_array = np.full((4,data.shape[0],data.shape[1]),np.nan).tolist()
            h_rp_values[key] = copy.deepcopy(h_empty_array)

    ### Calculation has to be done for every gridcell individually. As it is 
    ### rearanged also has to be individually -> can possibly be coded faster
    for column in range(data.shape[0]):
        for row in range(data.shape[1]):
            rp_dict = {}
            hist_rp_dict = {}
            rp_check = 0
            ### Sort values to different return periods
            for bootstrap in range(50):
                for rp in range(len(return_periods)):
                    try: ### Skip the faulty bootstrap values
                        rp_value = data[column][row][bootstrap][rp][0]
                    except TypeError:
                        continue
                    pr = data[column][row][bootstrap][rp][1]
                    # if pr > 400:
                    #     breakpoint()
                    if (scenario in ['rcp85_moc', 'rcp85_eoc'] and
                            cc_calc == 'yes'):
                        try:
                            h_pr = \
                            hist_data[column][row][bootstrap][rp][1]
                        except TypeError:
                            continue

                    if rp_check == 0:
                        rp_dict[return_periods[rp]] = [pr]
                        if (scenario in ['rcp85_moc', 'rcp85_eoc'] 
                            and cc_calc == 'yes'):
                            hist_rp_dict[return_periods[rp]] = [h_pr]
                    else:
                        rp_dict[return_periods[rp]] += [pr]
                        if (scenario in ['rcp85_moc', 'rcp85_eoc'] 
                            and cc_calc == 'yes'):
                            hist_rp_dict[return_periods[rp]] += [h_pr]

                    if rp == (data[column][row][bootstrap].shape[0]-1):
                        rp_check += 1

            ### Calc mean, median and uncertainty ranges for RP
            for key in rp_dict.keys():

                value_copy = copy.deepcopy(rp_dict[key])
            
                # if np.median(rp_dict[key]) < 0:
                #     breakpoint()
                # rp_values[key][0][column][row] = \
                #     copy.deepcopy(np.mean(rp_dict[key]))
                # rp_values[key][1][column][row] = \
                #     copy.deepcopy(np.median(rp_dict[key]))
                # rp_values[key][2][column][row] = \
                #     copy.deepcopy(np.percentile(rp_dict[key],10))
                # rp_values[key][3][column][row] = \
                #     copy.deepcopy(np.percentile(rp_dict[key],90))
                
                         
                if (scenario in ['rcp85_moc', 'rcp85_eoc'] and 
                    cc_calc == 'yes'):
                    cc_values = copy.deepcopy(((np.array(value_copy) - 
                                    np.array(hist_rp_dict[key])) / 
                                    np.array(hist_rp_dict[key])) * 100)
                    h_rp_values[key][0][column][row] = \
                        copy.deepcopy(np.mean(cc_values))
                    h_rp_values[key][1][column][row] = \
                        copy.deepcopy(np.median(cc_values))
                    h_rp_values[key][2][column][row] = \
                        copy.deepcopy(np.percentile(cc_values,10))
                    h_rp_values[key][3][column][row] = \
                        copy.deepcopy(np.percentile(cc_values,90))
            
    skaling = {}
    if scenario in ['rcp85_moc', 'rcp85_eoc'] and temp_scaling == 'yes':
        sk_data[sk_data == 0] = np.nan
          
        for key in return_periods:
            skaling_empty = np.full((2,sk_data.shape[0],sk_data.shape[1]),
                                    np.nan)
            skaling[key] = copy.deepcopy(skaling_empty)
            try:
                skaling[key][0] = np.array(h_rp_values[key][0]) / sk_data
                skaling[key][1] = np.array(h_rp_values[key][1]) / sk_data
            except:
                print(cpm, season) ### CMCC makes problemos
                breakpoint()
    print('Time for rearanging values and calculate mean, median, 10 and 90'
          +' percentiles for each gridcell: ' + 
            '%.2f' % (time.time() - t_beg) + ' s')

    if scenario in ['rcp85_moc', 'rcp85_eoc'] and temp_scaling == 'yes':
        try:
            rm_old_file='rm '+path_data+f'/Skaling_{cpm}_{region}_{season}.npy'
            os.system(rm_old_file)
        except:
            pass
        c_file = open(path_data + f'/Skaling_{cpm}_{region}_{season}.npy',"wb")
        pickle.dump(skaling, c_file)
        c_file.close()

    if scenario in ['rcp85_moc', 'rcp85_eoc'] and cc_calc == 'yes':
        try:
            rm_old_file = 'rm ' + path_data + f'/CC_{cpm}_{region}_{season}.npy'
            os.system(rm_old_file)
        except:
            pass
        b_file = open(path_data + f'/CC_{cpm}_{region}_{season}.npy', "wb")
        pickle.dump(h_rp_values, b_file)
        b_file.close()

    else:
        print("Please set 'temp_scaling' and 'cc_calc' to 'yes'")


    return identifier

### Parallelize at least some part to make it a teeny,tiny bit faster
for region in ['CH', 'CHNE', 'CHW', 'CHS' ,'CHAE', 'CHAW']:
# for region in ['CH']:
    for season in ['DJF', 'MAM', 'JJA', 'SON']:
    # for season in ['JJA']:
        print((season).center(60,'~'))
        # for i, val in enumerate(Institutes):
        #     rearange_reduce(1,Institutes[i])
        with Pool() as pool:
            pool.starmap(rearange_reduce, enumerate(Institutes))
    
                        




