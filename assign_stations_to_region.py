#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Uses metadata of stations to assign them to each region of interest.

"""
import os
import warnings
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
import scipy.interpolate.ndgriddata as ndgriddata
from matplotlib.colors import LinearSegmentedColormap

warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

### evaluation, historical
scenario = 'evaluation'

time_res = '1d'

### open region and station data
path_stations = '/users/lgimmi/MeteoSwiss/data' 
path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
if time_res == '1hr':
    filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                     +'allstats_andpartnerstats_withNA.csv')
if time_res == '1d':
    filename_stations = (path_stations + '/station_metadat_rre150d0_2000_2009_'
                         +'allstatswithpartnerstats_allNA.csv')
    
filename_regions = (path_region + '/station_region_latlon.nc')
plot_path = '/users/lgimmi/MeteoSwiss/figures/'
region = xr.open_dataset(filename_regions)
ch = region.orog
data = pd.read_csv(filename_stations)

lon = data['longitude']
lat = data['latitude']
df_stations = pd.DataFrame()
regions = ['CHNE', 'CHW', 'CHS' ,'CHAE', 'CHAW']


### Create figure to check where stations are found
map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
rad_earth = 6371.0  # approximate radius of Earth [km]
dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(map_ext[2:].mean()))) \
         / 360.0 * (map_ext[1] - map_ext[0])
dist_y = 2.0 * np.pi * rad_earth / 360.0 * (map_ext[3] - map_ext[2])
# -----------------------------------------------------------------------------
fig = plt.figure(figsize=(12.0, 12.0 * (dist_y / dist_x)))
ax = plt.axes(projection=ccrs.PlateCarree())
colors = {"CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
          "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

ax.coastlines(color='grey', linewidth=0.5)
ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
    linewidth = 0.5 )
ax.set_extent(map_ext, crs=ccrs.PlateCarree())
ax.set_aspect("auto")
# breakpoint()
for i, key in enumerate(regions):
        i+=1
        cmap = LinearSegmentedColormap.from_list(
        'custom_colormap',[colors[key], (1, 1, 1)] ,N=2 )

        col = ax.contourf(ch.lon, ch.lat, xr.where(ch == i, 1, np.nan),
        cmap=cmap,alpha=0.8)

### Check in which region (rounded) lat/lon of stations are found -> save info
for i, station in enumerate(data['nat_abbr']):

    if (scenario == 'evaluation' and station in 
            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
            'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
            'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
            'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
            'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
            'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
            'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
            'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
            'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
            'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
            'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
            'ZHWIN','ZHZEL'
            ]):
            continue
    if (scenario == 'evaluation' and time_res == '1d' and station in 
            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
             'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
             'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
             'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
             'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
             'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
             'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
             'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
             'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
             'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
             'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
             'ZHWIN','ZHZEL','BAC','BLA','BOV','BSW','BZL','CHX','CZG','GEMIR',
             'GWA','CLA','KER','LBG','CRO','LOE','MAU','NYO','EVL','OBW','PSB',
             'RIG','FRA','SET','GEBTQ','VDORN','WAW','ZHNIE','HER','ZZG','LEN',
             'LON','MUO','NEB','RHN','RIE','STF','TDO','THI','TILOD','TISOM',
             'UER','VDARP','VDAVS','VDCHX','VDHON','VDMOL','VDPEU','VDREN',
             'VIO','WAB','GRA','MDO','SCD','VAE','BEC','CEV','LAB','LAT','SZB',
             'ZOF','AAF','AHO','ARL','ASH','ATN','AUF','AUG','BAD','BHO','BNW',
             'BOA','BWI','DID','ERL','ERN','GEI','GLI','GNB','KIG','LPG','LTH',
             'MEB','MRW','MUZ','RAP','RUM','SCS','SSU','STI','UAG','UET','WAF'
             ]):
            continue
    if (scenario == 'historical' and station in 
            ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
            'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
            'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
            'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
            'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
            'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
            'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
            'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
            'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
            'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
            'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
            'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
            'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
            'TIBED','TIOLI'
            ]):
            continue
    
    for region_number in range(5):   
        region_number += 1     
        masked_region = ch.where(ch == region_number, drop = True)
        check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
        check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
        if check_lon in masked_region.lon and check_lat in masked_region.lat:
            idx_lon = np.where(masked_region.lon.values == check_lon)[0][0]
            idx_lat = np.where(masked_region.lat.values == check_lat)[0][0]
            if masked_region.values[idx_lat][idx_lon] == region_number:
                try:
                    df_stations[station] = [regions[region_number-1]]
                except:
                    breakpoint()
                break

    ax.plot(check_lon, check_lat, 'ko') 

df_stations.to_csv(path_region + f'/stations_region_{scenario}_{time_res}.csv',
                       index=False)

plt.savefig(os.path.join(plot_path, f'Station_location_{scenario}.pdf'),
            format='pdf', bbox_inches='tight')
plt.close(fig)



