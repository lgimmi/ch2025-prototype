#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import string
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical 
scenario = 'evaluation'

### xHourly, xDaily
time_res = '1hr'

### Seasons: DJF, MAM, JJA, SON
season = 'DJF'

### Single cell near station or all available griddcells (near_station / all)
compare_what = 'all'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
# return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])

'''-------------------------------------------------------------------------'''
### ------------------------------- CPM ----------------------------------- ###
if time_res == '1hr':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/{scenario}/'
elif time_res == '1d':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/{scenario}/'
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

Institutes = ['obs','CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['obs','CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
        'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
        'RegCM4-7']

### ------------------------------- RCM ----------------------------------- ###
RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
             +f'{scenario}')
path_region = '/scratch/snx3000/lgimmi/store/rcm/masks'
if scenario == 'evaluation':# and time_res == '1hr':
    RCM_Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
    RCMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
            'RegCM4-7']
# elif scenario == 'evaluation' and time_res == '1d':
#     RCM_Institutes = ['CNRM','KNMI','HCLIMcom','ICTP']
#     RCMs = ['ALADIN62','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
else:
    RCM_Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
    RCMs = ['COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
            'RegCM4-7']
if (scenario == 'evaluation' and time_res == '1hr' and compare_what == 'near_station'):
    RCM_Institutes = ['CLMcom-BTU','CLMcom-ETH','KNMI','HCLIMcom','ICTP']
    RCMs = ['CCLM5-0-9','COSMO-crCLIM','RACMO23E','HCLIM38-ALADIN',
            'RegCM4-7']

'''-------------------------------------------------------------------------'''
region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}
alphabet = list(string.ascii_lowercase)

if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
    plot_path = plot_path[:-11] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'
    plot_path = plot_path[:-11] + '/eoc'
    
for iterate, region in enumerate(region_numbers.keys()):
    print(f'{iterate+1}/{len(region_numbers.keys())}')
    fig = plt.figure(figsize=(11, 7.333))
    gs = fig.add_gridspec(nrows=3, ncols=4)
    row, col, bet = 0, 0, 0
    ### Set maximum y-value for plot
    if time_res == '1hr':
        if season == 'DJF':
            max_y = 50
        if season == 'DJF' and scenario in ['historical','evaluation']:
            max_y = 30
        elif season == 'JJA':
            max_y = 100
        elif season == 'MAM':
            max_y = 100
        elif season == 'SON':
            max_y = 60

    elif time_res == '1d':
        if season == 'DJF':
            max_y = 150
        elif season == 'JJA':
            max_y = 200
        elif season == 'MAM':
            max_y = 200
        elif season == 'SON':
            max_y = 200

    ### open region and station data
    path_stations = '/users/lgimmi/MeteoSwiss/data' 
    path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    if time_res == '1hr':
        fname_metadata = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                            +'allstats_andpartnerstats_withNA.csv')
    elif time_res == '1d':
        fname_metadata = (path_stations + '/station_metadat_rre150d0_2000_'
                             +'2009_allstatswithpartnerstats_allNA.csv')
        
    data = pd.read_csv(fname_metadata)
    lon = data['longitude']
    lat = data['latitude']

    filename_station = (path_save_GEV + f'/Values_stations_{season}_'
                        +f'{scenario}_{time_res}.npy')
    filename_region = (path_region_stations + 
                       f'/stations_region_{scenario}_{time_res}.csv')
    station_data = np.load(filename_station, allow_pickle=True)
    station_region = pd.read_csv(filename_region)
    st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []

    for rp in return_periods:
        st1, st2, st3, st4 = [],[],[],[]
        for i_station, station in enumerate(station_region.keys()):
            if station_region.values[0][i_station] != region and region != 'CH':
                continue
            try:
                st1.append(station_data[station][rp][2])
                st2.append(station_data[station][rp][3])
                st3.append(station_data[station][rp][0])
                st4.append(station_data[station][rp][1])
            except:
                breakpoint()
        
        st_ten_perc.append(np.nanmean(st1))
        st_ninety_perc.append(np.nanmean(st2))
        st_mean.append(np.nanmean(st3))
        st_median.append(np.nanmean(st4))

    lon_list = []
    lat_list = []
    for i, station in enumerate(data['nat_abbr']):

        if (scenario == 'evaluation' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL'
                ]):
                continue
        if (scenario == 'historical' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                'TIBED','TIOLI'
                ]):
                continue
            
        # masked_region = ch.where(ch == region_numbers[region], drop = True)
        # check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
        # check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
        check_lon = lon[i]
        check_lat = lat[i]
        lon_list.append(check_lon)
        lat_list.append(check_lat)


    maxima_list = []
    r_maxima_list = []
    for i_cpm, cpm in enumerate(Institutes):
        if cpm == 'obs':
            continue
        # elif cpm in ['CLMcom-CMCC'] and compare_what == 'near_station': # 'CNRM'
        #     col+=1
        #     continue

        ax = fig.add_subplot(gs[row,col])

        # if scenario == 'rcp85_moc':
        #     path_data = path_data[:-4] + '/moc'
        # elif scenario == 'rcp85_eoc':
        #     path_data = path_data[:-4] + '/eoc'

        if compare_what == 'near_station' and region != 'CH':
            filename  = (path_data + f'/StatRegrid_{cpm}_{time_res}_RP_prmax_{season}.nc')
            ds = xr.open_dataset(filename)
            da = ds[region]
            steps = (da.lon[1] - da.lon[0]).values

        elif compare_what == 'near_station' and region == 'CH':
            filename  = (path_data + f'/StatRegrid_CH_{cpm}_{time_res}_RP_prmax_{season}.nc')
            ds = xr.open_dataset(filename)
            da = ds[region]
            steps = (da.lon[1] - da.lon[0]).values

        else:
            filename = (path_data + '/' + f'Values_{cpm}_{region}_{season}.npy')
            data = np.load(filename, allow_pickle=True)
        

        ten_perc, ninety_perc, median, mean, local = [], [], [], [], []
        
        for rp in return_periods:
            if compare_what == 'near_station':
                rp = da.returnperiod.values.tolist().index(rp)
                data = da 
            value0, value1, value2, value3 = \
                data[rp][0], data[rp][1], data[rp][2], data[rp][3]

            
            ### Only select gridcell that is closest to station
            if compare_what == 'near_station':
                list0, list1, list2, list3 = [], [], [], []
                for ind in range(len(lon_list)):
                    lat_val, lon_val = lat_list[ind], lon_list[ind]
                    lon0 = value0.sel(lon=lon_val, method='nearest')
                    lon1 = value1.sel(lon=lon_val, method='nearest')
                    lon2 = value2.sel(lon=lon_val, method='nearest')
                    lon3 = value3.sel(lon=lon_val, method='nearest')
                    lat0 = lon0.sel(lat=lat_val, method='nearest')
                    lat1 = lon1.sel(lat=lat_val, method='nearest')
                    lat2 = lon2.sel(lat=lat_val, method='nearest')
                    lat3 = lon3.sel(lat=lat_val, method='nearest')
                    list0.append(lat0.values)
                    list1.append(lat1.values)
                    list2.append(lat2.values)
                    list3.append(lat3.values)
                    # print('\n',lat_list[ind], lon_list[ind], ' | ', lat0.lat.values, lat0.lon.values, '\n')

                value0, value1, value2, value3 = \
                    np.array(list0), np.array(list1), np.array(list2), np.array(list3)

            ten_perc.append(np.nanmean(value2))
            ninety_perc.append(np.nanmean(value3))
            mean.append(np.nanmean(value0))
            median.append(np.nanmean(value1))
            if rp == return_periods[-1]:
                maxima_list.append(np.nanmean(value3))
        
        ### Add conventional regional climate models
        if cpm in RCM_Institutes:
            if compare_what == 'near_station' and region != 'CH':
                r_fname  = (RCM_path_data + f'/StatRegrid_{cpm}_{time_res}_RP_prmax_{season}.nc')
                ds = xr.open_dataset(r_fname)
                da = ds[region]

            elif compare_what == 'near_station' and region == 'CH':
                r_fname  = (RCM_path_data + f'/StatRegrid_CH_{cpm}_{time_res}_RP_prmax_{season}.nc')
                ds = xr.open_dataset(r_fname)
                da = ds[region]

            else:
                r_fname = (RCM_path_data + f'/Values_{cpm}_{region}_{season}.npy')
                r_data = np.load(r_fname, allow_pickle=True)

            r_ten_perc, r_ninety_perc, r_median, r_mean = [], [], [], []
            
            for rp in return_periods:
                if compare_what == 'near_station':
                    rp = da.returnperiod.values.tolist().index(rp)
                    r_data = da 
                r_value0, r_value1, r_value2, r_value3 = \
                    r_data[rp][0], r_data[rp][1], r_data[rp][2], r_data[rp][3]

                ### Only select gridcell that is closest to station for plot
                if compare_what == 'near_station':
                    list0, list1, list2, list3 = [], [], [], []
                    for ind in range(len(lon_list)):
                        lat_val, lon_val = lat_list[ind], lon_list[ind]
                        lon0 = r_value0.sel(lon=lon_val, method='nearest')
                        lon1 = r_value1.sel(lon=lon_val, method='nearest')
                        lon2 = r_value2.sel(lon=lon_val, method='nearest')
                        lon3 = r_value3.sel(lon=lon_val, method='nearest')
                        lat0 = lon0.sel(lat=lat_val, method='nearest')
                        lat1 = lon1.sel(lat=lat_val, method='nearest')
                        lat2 = lon2.sel(lat=lat_val, method='nearest')
                        lat3 = lon3.sel(lat=lat_val, method='nearest')
                        list0.append(lat0.values)
                        list1.append(lat1.values)
                        list2.append(lat2.values)
                        list3.append(lat3.values)
                    r_value0, r_value1, r_value2, r_value3 = \
                        np.array(list0), np.array(list1), np.array(list2), np.array(list3)

                r_ten_perc.append(np.nanmean(r_value2))
                r_ninety_perc.append(np.nanmean(r_value3))
                r_mean.append(np.nanmean(r_value0))
                r_median.append(np.nanmean(r_value1))
                if rp == return_periods[-1]:
                    r_maxima_list.append(np.nanmean(r_value3))

            ax.plot(return_periods, r_median, label='median (parent RCM)',
                        color='crimson', linestyle='-', zorder=-10)
            ax.fill_between(return_periods,r_ten_perc,r_ninety_perc,
                label = r'10$^{th}$-90$^{th}$P '+
                '(parent RCM)', color='lightcoral', alpha=0.2, zorder=-20)
        
        # for i in [ten_perc,ninety_perc,mean,median]:
        #     local.append(i)

        # try:
        #     rm_old_file = ('rm ' + '/users/lgimmi/MeteoSwiss/local' + 
        #         f'/local_{cpm}_{region}_{season}.npy')
        #     os.system(rm_old_file)
        # except:
        #     print('No old file to remove')

        # np.save('/users/lgimmi/MeteoSwiss/local/' +
        #         f'local_{cpm}_{region}_{season}.npy',
        #         np.array(local, dtype=object), allow_pickle=True)

       
        ### Add Station data
        ax.plot(return_periods, st_median, label='median (Stations)', color='k',
                linestyle='-')
        ax.plot(return_periods, st_ten_perc,
                 label=r'10$^{th}$-90$^{th}$P ' +
                 '(Stations)', color='k', linestyle='--',
                 linewidth=0.5,zorder=-5)
        
        ax.plot(return_periods, st_ninety_perc, color='k', linestyle='--',
                linewidth=0.5,zorder=-5)
        
        ### Add CPMs
        ax.plot(return_periods, median, label='median (CPM)',
                 color='dodgerblue', linestyle='-')
        # ax.fill_between(return_periods,st_ten_perc,st_ninety_perc,
        #                 label = r'10$^{th}$-90$^{th}$P', color='k',
        #                 alpha=0.3,zorder=-25)
        ax.fill_between(return_periods,ten_perc,ninety_perc,
                        label = r'10$^{th}$-90$^{th}$P ' 
                        +'(CPM)', color='lightblue',
                        alpha=0.5,zorder=-15)
        # ax.text(23, max_y+(1/25*max_y), f'{cpm}',fontsize=11,
        #                 #weight='bold',
        #             verticalalignment='center', horizontalalignment='center',
        #             rotation=0) 
        ax.set_title(f'{cpm}', x=0.02, fontsize=13, loc='left')
        ax.set_title(f'{alphabet[bet]}',fontsize=12, loc='right',x=0.98 )       

        major_ticks_y = np.arange(0, max_y+1, max_y/10*2)
        minor_ticks_y = np.arange(0, max_y+1, max_y/10)
        # ax.set_yticks(major_ticks_y, fontsize=9)
        ax.set_yticks(minor_ticks_y, minor=True, fontsize=8)

        ax.grid(which='both', axis='y', alpha=0.3)

        if row == 2:
            ax.set_xlabel('Returnperiod [yr]',fontsize=13)
        elif row == 1 and col in [2,3]:
            ax.set_xlabel('Returnperiod [yr]',fontsize=13)
        if col == 0:
            ax.set_ylabel('Intensity [mm/h]',fontsize=13)
        
        ax.set_xscale('log')
        ax.set_xticks([5,10,20,50,100], fontsize=8)
        ax.get_xaxis().set_major_formatter(ScalarFormatter())

        ax.set_ylim(0,max_y)
        plt.xlim(return_periods[0],return_periods[-1])
        if col == 3:
            col = 0
            row += 1
        else:
            col += 1
        bet += 1
        # Mock entry
        ax.plot(np.nan,np.nan, 'ko',markersize=3, label = 'station locations')
        

    handles, labels = plt.gca().get_legend_handles_labels()
    order = [0,1,2,3]
    if cpm in RCM_Institutes:
        # order = [5,2,3,4,0,1]
        order = [6,2,3,4,5,0,1]
    # fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
    #            bbox_to_anchor=(0.67, 0.19), frameon=False, fontsize=14)
    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.935, 0.32), frameon=False, fontsize=11)
    
    if scenario == 'evaluation':
        plt.suptitle(f'Evaluation | {season_names[season]} | {time_res} | {region}',
                      y=0.965, fontsize=16)
    elif scenario == 'historical':
        plt.suptitle(f'Historical | {season_names[season]} | {time_res} | {region}',
                      y=0.965, fontsize=16)

    ### --------------------------------------------------------------------###
    ### Add overview of region and stations to plot
    ### --------------------------------------------------------------------###

    ax = fig.add_subplot(gs[row,col], projection=ccrs.PlateCarree())
    map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
        linewidth = 0.5 )
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.set_aspect("auto")

    ### open region and station data
    path_stations = '/users/lgimmi/MeteoSwiss/data' 
    path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    if time_res == '1hr':
        filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                            +'allstats_andpartnerstats_withNA.csv')
    elif time_res == '1d':
        filename_stations = (path_stations + '/station_metadat_rre150d0_2000_'
                             +'2009_allstatswithpartnerstats_allNA.csv')
        
    if region != 'CH':
        filename_regions = (path_region_stations + '/station_region_latlon.nc')
    else:
        filename_regions = (path_region_stations + 
                            '/station_region_latlon_maskCH.nc')

    regions = xr.open_dataset(filename_regions)
    if region != 'CH':
        ch = regions.orog
    else:
        ch = regions['mask_CH']
    data = pd.read_csv(filename_stations)
    lon = data['longitude']
    lat = data['latitude']

    colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
            "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}
    # colors = {"CH":"#C31616", "CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
    #         "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

    cmap = LinearSegmentedColormap.from_list('custom_colormap', [colors[region],
                                            (1, 1, 1)] , N=2 )
    
    masked_region = ch.where(ch == region_numbers[region], drop=True)
    if compare_what != 'near_station':
        if region == 'CH': 
            col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                        cmap=cmap, alpha=1.0)
            # ax.plot(masked_region.lon[114:115],masked_region.lat[53:54],marker='P',
            #         color='white', transform=ccrs.PlateCarree(),markersize=30)
        else:
            col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=0.8)

    ### Check in which region (rounded) lat/lon of stations are found
    num_of_stations = 0
    for i, station in enumerate(data['nat_abbr']):

        if (scenario == 'evaluation' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL'
                ]):
                continue
        if (scenario == 'historical' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                'TIBED','TIOLI'
                ]):
                continue
            
        # masked_region = ch.where(ch == region_numbers[region], drop = True)
        check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
        check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
        if check_lon in masked_region.lon and check_lat in masked_region.lat:
            idx_lon = np.where(masked_region.lon.values == check_lon)[0][0]
            idx_lat = np.where(masked_region.lat.values == check_lat)[0][0]
            # value_lon = masked_region.where(masked_region.lon == check_lon)
            # value = masked_region.where(masked_region.lat == check_lat)
            if masked_region.values[idx_lat][idx_lon]==region_numbers[region]:
                # ax.pcolormesh([masked_region.lon[idx_lon]-steps,
                #                masked_region.lon[idx_lon], 
                #                masked_region.lon[idx_lon]+steps],

                #                 [masked_region.lat[idx_lat]-steps,
                #                  masked_region.lat[idx_lat],
                #                  masked_region.lat[idx_lat]+steps],
                                 
                #                 [masked_region[idx_lat][idx_lon],
                #                  masked_region[idx_lat][idx_lon],
                #                  masked_region[idx_lat][idx_lon]],
                #                 shading='nearest',cmap=cmap)
                if time_res == '1hr':
                    ax.plot(check_lon, check_lat, 'ko',markersize=2) 
                    # if compare_what == 'near_station':
                    #     ax.plot(check_lon, check_lat, 's',markersize=3,
                    #             color=colors[region])
                    ax.plot(check_lon, check_lat, 'ko',markersize=1)
                elif time_res == '1d':
                    ax.plot(check_lon, check_lat, 'ko',markersize=1)
                    # if compare_what == 'near_station':
                    #     ax.plot(check_lon, check_lat, 's',markersize=3,
                    #             color=colors[region])
                num_of_stations += 1

    ### --------------------------------------------------------------------###

    ax.text(0.97, 0.02, f'#of stations: {num_of_stations}',fontsize=9,
             weight='bold', 
            verticalalignment='bottom', horizontalalignment='right',
            rotation=0, transform=ax.transAxes)
    

    plt.subplots_adjust( wspace=0.3, hspace=0.35)

    if compare_what == 'near_station':
        if scenario == 'historical':
            plt.savefig(os.path.join(plot_path, f'Gridcell_hist_GEV_{season}_{region}.pdf'),
                        format='pdf',bbox_inches='tight')
        else:
            plt.savefig(os.path.join(plot_path, f'Gridcell_GEV_{season}_{region}.pdf'),
                        format='pdf',bbox_inches='tight')
    else:
        if scenario == 'historical':
            plt.savefig(os.path.join(plot_path, f'hist_GEV_{season}_{region}.pdf'),
                        format='pdf',bbox_inches='tight')
        else:
            plt.savefig(os.path.join(plot_path, f'GEV_{season}_{region}.pdf'),
                        format='pdf',bbox_inches='tight')
            
    # plt.show()
    # breakpoint()
    plt.close(fig)
