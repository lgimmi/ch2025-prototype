#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Create ncfile from maxima precipitation values of each returnperiod.

"""
import os, sys
import glob
import time
import copy
import warnings
import numpy as np
import xarray as xr 
import matplotlib as mpl
import cartopy.crs as ccrs
from natsort import natsorted
import matplotlib.pyplot as plt
from multiprocessing import Pool
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'historical'

### xHourly, xDaily
time_res = '1d'

### Seasons: DJF, MAM, JJA, SON
seasons = ['DJF', 'MAM', 'JJA', 'SON']

### CH2018 regions: CH, CHNE, CHW, CHS, CHAE or CHAW
## region = 'CHAW'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
return_periods = np.append(return_periods, [150, 200, 250, 300])
# return_periods = np.arange(2,201,1)


### Which Returnperiod do you want to look at
my_rp = 5

'''-------------------------------------------------------------------------'''
## ------------------------------- CPM ----------------------------------- ###

path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

data_path_prdata = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr/{scenario}'

mask_path_regions = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

# Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
# CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
#         'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
#         'RegCM4-7']
Institutes = ['CLMcom-BTU']
CPMs = ['CCLM5-0-14']


rcm = None

### ------------------------------- RCM ----------------------------------- ###
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')
# path_save_GEV = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')
# path_region = '/scratch/snx3000/lgimmi/store/rcm/masks'

# data_path_prdata = f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr/{scenario}'

# mask_path_regions = '/scratch/snx3000/lgimmi/store/rcm/masks'

# if scenario == 'evaluation':
#     Institutes = ['CLMcom-ETH','CLMcom-BTU','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-9','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
# else:
#     Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']

# rcm = 1

'''-------------------------------------------------------------------------'''
if time_res == '1hr':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/maps/'
elif time_res == '1d':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/maps/'

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}
region_list = ['CH', 'CHAE', 'CHAW', 'CHW', 'CHNE', 'CHS']
stats = ['Mean','Median','10P','90P']
my_rp_ind = return_periods.tolist().index(my_rp)

count_value = np.arange(1,
            len(region_numbers.keys())*len(seasons)*(len(Institutes)-1)+1,1)
iter_value = iter(count_value)

if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'

def overview_plot(da, values):
    # '''-----------------------------------------------------------'''
    # ## Quick plot to check if putting Dataset together worked
    # '''-----------------------------------------------------------'''
    ### Most models are in rotated lat lon grid
    rlat,rlon,pole = da.rlat, da.rlon, da.rotated_pole
    # rlon = da.rlon
    # pole = da.rotated_pole
    try:
        if hasattr(pole,'grid_north_pole_longitude'):
            pole_lon = pole.attrs['grid_north_pole_longitude']
        if hasattr(pole,'grid_north_pole_latitude'):
            pole_lat = pole.attrs['grid_north_pole_latitude']
    except:
        print('Unexpected error:', sys.exc_info()[0])
        raise
    crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

    # map_ext = np.array([-180, 180, -90, 90])  # [degree]
    # map_ext = np.array([-0, 11, 45, 49])  # [degree]
    map_ext = np.array([5.3, 10.7, 45.6, 48.0])  # [degree]
    rad_earth = 6371.0  # approximate radius of Earth [km]
    dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
        map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
    dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

    fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                        subplot_kw={'projection':ccrs.PlateCarree()})
    # fig, ax = plt.subplots(figsize=(12.0 , 12.0),
    # subplot_kw={'projection':ccrs.PlateCarree()})
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
    ax.set_aspect("auto")
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.contourf(rlon, rlat, values, cmap='jet', transform=crs)
    
    # plt.show()
    # breakpoint()

for i_ses, season in enumerate(seasons):
    print((season).center(50,'-'))
    for i_cpm, cpm in enumerate(Institutes):
        checkvalue = 0
        if cpm == 'obs':
            continue 
        # if cpm == 'CNRM' and rcm == 1:
        #     continue 
        # if cpm == 'CLMcom-CMCC' and rcm == None:
        #     continue 
        print(cpm)
        ### Create output array
        region_coords = xr.DataArray(region_list, dims=['region'])
        return_period_coords = xr.DataArray(return_periods, dims=['returnperiods'])
        median_coords = xr.DataArray(stats, dims=['stats'])

        data_regions = []

        for i_region, region in enumerate(region_numbers.keys()):
            # print(region)
            # now_value = next(iter_value)
            # print(f'{now_value}/{count_value[-1]}')
            i_region = region_numbers[region]

            maxima_list = []
            r_maxima_list = []

            if scenario == 'rcp85_moc':
                path_data = path_data[:-4] + '/moc'
                data_path_prdata = data_path_prdata[:-4] + '/moc'
            elif scenario == 'rcp85_eoc':
                path_data = path_data[:-4] + '/eoc'
                data_path_prdata = data_path_prdata[:-4] + '/eoc'

            # filename_npy = (path_data + '/' + f'OneYearStep_Values_{cpm}_{region}_{season}.npy')
            filename_npy = (path_data + '/' + f'Values_{cpm}_{region}_{season}.npy')
            data_npy = np.load(filename_npy, allow_pickle=True)

            '''-----------------------------------------------------------------'''
            ### GET CORRECT MAP AS NC FILE
            ### Get filenames
            if cpm == 'KNMI':
                filename = (data_path_prdata + '/' + f'*{CPMs[i_cpm]}*')
            elif cpm == 'MOHC':
                filename = (data_path_prdata + '/' + f'*{CPMs[i_cpm]}*')
            elif cpm == 'CLMcom-KIT' and scenario == 'evaluation':
                filename = (data_path_prdata + '/' + f'*{cpm}**CCLM5-0-14*')
            else:
                filename = (data_path_prdata + '/' + f'*{cpm}**{CPMs[i_cpm]}*')
            filenames_list = natsorted(glob.glob(filename))
            ds = xr.open_mfdataset(filenames_list[0],use_cftime=True)
            da = ds.sel(time=ds.time[:2])

            ### open mask file for regions
            if region == 'CH':
                endname = 'maskCH'
                variable_mask = 'mask_CH'
            else:
                endname = 'Ch2018'
                variable_mask = 'orog'

            if cpm == 'MOHC':
                region_file = (mask_path_regions+ f'/*{CPMs[i_cpm]}*{endname}*')
            elif cpm == 'CNRM' and scenario == 'historical' and rcm == 1:
                region_file = (mask_path_regions + 
                                f'/{cpm}*{CPMs[i_cpm]}*{endname}*')
            elif (cpm == 'ETH' and scenario == 'evaluation' and rcm == 1 and
                    time_res == '1d'):
                region_file = (mask_path_regions + 
                                f'/ETH_oneday_*{endname}*')
            # elif (cpm in ['KNMI', 'HCLIMcom'] and scenario == 'evaluation' and
            #       rcm == 1 and time_res == '1d'):
            #     region_file = (mask_path_regions +
            #                    f'/*{cpm}*{CPMs[i_cpm]}*{endname}*remapped*')
            else:
                region_file = (mask_path_regions + 
                                f'/*{cpm}*{CPMs[i_cpm]}*{endname}*')
                # region_file = (mask_path_regions + '/test_CMCC_maskCH.nc')
            # if cpm == 'KNMI':
            #     breakpoint()
            ds_region = xr.open_mfdataset(region_file)
            mask_region = ds_region[variable_mask]
            ds_region.close()

            ### Allign masks to dimensions and cooridinates of CPM as they
            ###  differ slightly on the 7th or higher decimal place
            mask_region = \
                mask_region.expand_dims(dim={"time": len(da.time)})
            mask_region = mask_region.assign_coords(time=da.time)
            try:
                da, mask_region = xr.align(da, mask_region, join="override")    
            except:
                print(f'{cpm} has not alligned with its mask')
                breakpoint()
                pass
            # '''-----------------------------------------------------'''
            ### Diff. way to mask data (extent outside region is kept)
            ### Separate masking into blocks of size limited to 1.0GB
            ### to avoid freezing of code due to memory issues
            ### Used for CMCC model as it has coordinate problem
            # '''-----------------------------------------------------'''

            def masking(xarray_dataarray,  mask, max_block = 1.0):
                '''
                Separate masking into blocks of size limited to 1.0GB 
                (max_block) to avoid freezing of code due to memory issues

                -----
                Returns maked input array as numpy array

                '''
                global i_region

                block_size = max_block
                da = xarray_dataarray
                mask_region = mask
                block_size = 1.0
                
                len_time = da['time'].size
                if ('x' in list(da.coords)) and ('y' in list(da.coords)):
                    len_x = da['x'].size
                    len_y = da['y'].size
                    out_dim = {'y','x'}
                elif ('rlon' in list(da.coords)) and ('rlat' in list(da.coords)):
                    len_x = da['rlon'].size
                    len_y = da['rlat'].size
                    out_dim = {'rlat','rlon'}
                elif ('lon' in list(da.coords)) and ('lat' in list(da.coords)):
                    len_x = da['lat'].size
                    len_y = da['lon'].size
                    out_dim = {'lon','lat'}
                else:
                    print(f'The model {cpm} uses an unknown coordinate systems'
                        +f' -- {list(da.coords)} -- ')
                if cpm == 'CLMcom-CMCC' and time_res == '1hr':
                    len_x = ds['rlon'].size
                    len_y = ds['rlat'].size
                    out_dim = {'rlat','rlon'}
                elif cpm == 'CLMcom-CMCC' and time_res == '1d':
                    len_x = ds['x'].size
                    len_y = ds['y'].size
                    out_dim = {'rlat','rlon'}

                dataarray = np.empty((len_time, len_y, len_x), dtype=np.float32)
                dataarray.fill(np.nan)
                maskarray = np.empty((len_time, len_y, len_x), dtype=np.float32)
                maskarray.fill(np.nan)
                da_masked = np.empty((len_time, len_y, len_x), dtype=np.float32)
                da_masked.fill(np.nan)

                num_blocks = int(np.ceil((da_masked.nbytes/ (10 ** 9))/block_size))
                lim = np.linspace(0, len_time, num_blocks + 1, dtype=np.int32)  
                
                for i in range(num_blocks):
                    t_beg = time.time()
                    slice_t = slice(lim[i], lim[i + 1])
                    try:
                        dataarray[slice_t,:,:] = da[slice_t,:,:].values
                    except:
                        breakpoint()
                    maskarray[slice_t,:,:] = mask_region[slice_t,:,:].values

                    da_masked[slice_t,:,:] = np.where(
                        maskarray[slice_t,:,:] == float(i_region),
                        dataarray[slice_t,:,:], #* multiply, #change units to mm/hr
                        0.0
                        )
                    
                    print('Data blocks loaded: '+str(i + 1)+'/'+str(num_blocks))
                    print('Time for masking for this block: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')
                return da_masked

            # '''-------------------------------------------------------'''
            ### Masking using xarray can drop unnecessairy grid cells to 
            ### speed up code and free up memory (Not working with CMCC 1h)
            # '''-------------------------------------------------------'''   
            try:
                print(da['pr'].units)
            except:
                print(f'No units found in {cpm}')
                breakpoint()
            
            # if (da['pr'].units == 'kg m-2 s-1' and time_res == '1hr'):
            #     print('Units in mm/s, mean: ' + str(da['pr'].values.mean()))
            #     print('Adjusted to mm/h')
            #     # da = da * 3600
            # elif (da['pr'].values.mean() < 0.001 and da.units != 'kg m-2 s-1'):
            #     print(f'Check units of {cpm}')
            # # check units
            # try:
            #     if (cpm == 'MOHC' and time_res in ['1hr','3hr','6hr'] and rcm == None and
            #         scenario == 'rcp85_moc'):
            #         ### MOHC is missing units
            #         MOHC_unit = {'1hr': 'kg m-2 s-1'}
            #         da['pr'].attrs['units'] = MOHC_unit['1hr']
            # except:
            #     pass
            # try:
            #     if (cpm in ['CLMcom-ETH', 'CNRM', 'ICTP', 'CLMcom-CMCC'] and 
            #         time_res in ['1d','3d','5d'] and rcm == None and
            #         scenario in ['historical','rcp85_moc','rcp85_eoc']):
            #         ### MOHC is missing units
            #         MOHC_unit = {'1d': 'mm/d'}
            #         da['pr'].attrs['units'] = MOHC_unit['1d']
            # except:
            #     pass

            if time_res in ['1hr','3hr','6hr']:
                # multiply = 3600
                if time_res == '1hr':
                    maxima_threshold = 250
                elif time_res == '3hr':
                    maxima_threshold = 350
                elif time_res == '6hr':
                    maxima_threshold = 450
            elif time_res in ['1d','3d','5d']:
                # multiply = 1
                if time_res == '1d':
                    maxima_threshold = 1000
                elif time_res == '3d':
                    maxima_threshold = 1300
                elif time_res == '5d':
                    maxima_threshold = 1500


                # if cpm in ['CLMcom-ETH', 'CNRM', 'ICTP', 'CLMcom-CMCC']:
                #     multiply = 3600
                # elif (cpm == 'CLMcom-CMCC' and scenario == 'evaluation'
                #            and time_res == '1d' and rcm == None):
                #         multiply = 1
            ### Include some exceptions as some models did not adjust their 
            ### units correctly ore were calculated wrongly..should after this
            ### step be correct --- CHECK EVERYTIME IF STILL CORRECT
            # if da.pr.units != 'kg m-2 s-1' and time_res in ['1hr','3hr','6hr']: 
            #     print(f'Error with {cpm} - make sure to check its units')
            #     raise RuntimeError
            # if da.pr.units != 'mm/d' and time_res in ['1d','3d','5d']: 
            #     if da.pr.units == 'mm/h' and cpm == 'MOHC': 
            #         pass
            #     elif (da.pr.units == 'kg m-2 s-1' and cpm in ['CLMcom-ETH',
            #         'KNMI','HCLIMcom','ICTP','CNRM']):
            #         if checkvalue == 0:
            #             print(da.pr.units, da.pr.values.mean())
            #             print('multiplying by 3600')
            #             checkvalue += 1
            #         multiply = 3600
            #     else:
            #         print(f'Error with {cpm} - make sure to check its units')
            #         breakpoint()
            #         raise RuntimeError
                    

            if cpm in ['CLMcom-CMCC']:
                if (time_res == '1d' and scenario == 'evaluation'):
                    ds = ds.rename({'x':'rlon','y':'rlat'})
                    da = da.rename({'x':'rlon','y':'rlat'})
                    # breakpoint()
                    if 'rlat' not in da.coords:
                        filename_CMCC = ('/scratch/snx3000/lgimmi/store/ALP-3/1hr'
                                        +'/pr/evaluation/*CMCC*')
                        fnames_CMCC = natsorted(glob.glob(filename_CMCC))
                        ds_CMCC = xr.open_mfdataset(fnames_CMCC[0],
                                                    use_cftime=True)
                        da_CMCC = ds_CMCC.sel(time=ds_CMCC.time[:2])
                        da['rlat'] = da_CMCC['rlat']
                        da['rlon'] = da_CMCC['rlon']
                        ds['rlat'] = da_CMCC['rlat']
                        ds['rlon'] = da_CMCC['rlon']
                ### All of CH Mask file of CMCC is wrongly labeled
                try:
                    mask_region = mask_region.rename({'x':'rlon','y':'rlat'})
                    mask_region['rlon'] = ds['pr']['rlon']
                    mask_region['rlat'] = ds['pr']['rlat']
                except:
                    pass
                # breakpoint()
            
            t_beg = time.time()
            try:
                xrda_masked = da.where(mask_region == i_region,
                                    drop = True)
                da_masked = xrda_masked.sel(time=xrda_masked.time[0])
            except:
                da['rlat'] = mask_region['rlat']
                da['rlon'] = mask_region['rlon']
                ds['rlat'] = mask_region['rlat']
                ds['rlon'] = mask_region['rlon']
                xrda_masked = da.where(mask_region == i_region,
                                    drop = True)
                da_masked = xrda_masked.sel(time=xrda_masked.time[0])
                
        
            # '''-----------------------------------------------------------'''
            # ## Quick plot to check if masking dry areas worked
            # '''-----------------------------------------------------------'''
            ### Most models are in rotated lat lon grid
            # rlat = da.rlat
            # rlon = da.rlon
            # pole = da.rotated_pole
            # try:
            #     if hasattr(pole,'grid_north_pole_longitude'):
            #         pole_lon = pole.attrs['grid_north_pole_longitude']
            #     if hasattr(pole,'grid_north_pole_latitude'):
            #         pole_lat = pole.attrs['grid_north_pole_latitude']
            # except:
            #     print('Unexpected error:', sys.exc_info()[0])
            #     raise
            # crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)
            # # # map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
            # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
            # rad_earth = 6371.0  # approximate radius of Earth [km]
            # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

            # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
            #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # # fig, ax = plt.subplots(figsize=(12.0 , 12.0),
            # #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # ax.coastlines(color='grey', linewidth=0.5)
            # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
            # ax.set_aspect("auto")
            # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.contourf(da_masked.rlon, da_masked.rlat, da_masked.pr, 
            #     cmap='terrain', transform=crs)
            
            # plt.show()
            
            ds_new = da_masked
        
            data_arrays = []

            for return_period in return_periods:
                data = np.array(data_npy[return_period])
                data_shape = data.shape

                # Create data array with varying dimensions
                if (cpm == 'CLMcom-CMCC' and time_res == '1d' and
                     scenario == 'rcp85_eoc' and ('rlat' in ds_new.coords and 
                                                   'rlon' in ds_new.coords)):
                    data_array = xr.DataArray(data_npy[return_period],
                            dims=['stats','rlat', 'rlon'],
                            coords={'stats': median_coords,
                            'rlat': da.coords['rlat'],
                            'rlon': da.coords['rlon']})
                elif (cpm == 'CLMcom-CMCC' and scenario == 'historical' and
                      time_res == '1d' and rcm == None):
                    data_array = xr.DataArray(data_npy[return_period],
                            dims=['stats','rlat', 'rlon'],
                            coords={'stats': median_coords,
                            'rlat': da.coords['rlat'],
                            'rlon': da.coords['rlon']})
                    
                elif (cpm == 'CLMcom-CMCC' and 
                    ('rlat' in ds_new.coords and 'rlon' in ds_new.coords)):
                    data_array = xr.DataArray(data_npy[return_period],
                        dims=['stats','rlat', 'rlon'],
                        coords={'stats': median_coords,
                        'rlat': ds_new.coords['rlat'],
                        'rlon': ds_new.coords['rlon']})

                elif 'rlat' in ds_new.coords and 'rlon' in ds_new.coords:
                    data_array = xr.DataArray(data_npy[return_period],
                            dims=['stats','rlat', 'rlon'],
                            coords={'stats': median_coords,
                            'rlat': ds_new.coords['rlat'],
                            'rlon': ds_new.coords['rlon']})
                elif 'x' in ds_new.coords and 'y' in ds_new.coords:
                    data_array = xr.DataArray(data_npy[return_period],
                            dims=['stats','y', 'x'],
                            coords={'stats': median_coords,
                            'y': ds_new.coords['y'],
                            'x': ds_new.coords['x']})
                elif cpm == 'CNRM' and rcm == 1:
                    # breakpoint()
                    ds = ds.rename({'x':'lon','y':'lat'})
                    da = da.rename({'x':'lon','y':'lat'})
                    data_array = xr.DataArray(data_npy[return_period],
                            dims=['stats','lon','lat'],
                            coords={'stats': median_coords,
                            'lon': ds_new.coords['lon'],
                            'lat': ds_new.coords['lat']})
                    # data_array = xr.DataArray(data_npy[return_period],
                    #         dims=['stats','y','x'],
                    #         coords={'stats': median_coords,
                    #         'y': ds_new.y,
                    #         'x': ds_new.x})
                else:
                    breakpoint()

                data_arrays.append(data_array)
            ds_rp = xr.concat(data_arrays, dim=xr.DataArray(return_periods,
                                            dims=['returnperiod']))
            try:
                new_ds_region = ds_rp.reindex_like(ds.sel(time=ds.time[0]))
            except:
                breakpoint()

            da_masked[region] = new_ds_region
            if region == 'CH':
                # try:
                #     MyDataset = xr.Dataset({'rotated_pole': da.rotated_pole},
                #                        attrs=da.attrs)
                # except:
                #     print(f'{cpm} is not rotated pole')
                #     pass
                proc_info = ('Returnperiods of maximum precipitation for different'
                        +' regions of Switzerland. Following the methods of'
                        +'Ban, N., Rajczak, J., Schmidli, J. et al. Analysis of'
                        +' Alpine precipitation extremes using generalized'
                        +' extreme value theory in convection-resolving climate'
                        +' simulations. Clim Dyn 55, 61–75 (2020). '
                        +'https://doi.org/10.1007/s00382-018-4339-4')
                newDataset = copy.deepcopy(ds.sel(time=ds.time[0]))
                newDataset = newDataset.drop_vars('pr')
                try:
                    newDataset = newDataset.drop_vars('time_bnds')
                except:
                    pass
                newDataset.attrs["processing_information"] = proc_info
                newDataset[region] = new_ds_region 
                
            else:
                newDataset[region] = new_ds_region 
        ### CHECK UNITS, adjust units and adjust values if needed
        if time_res in ['1hr','3hr','6hr']:
            units = {f'{time_res}': 'mm/h'}
        elif time_res in ['1d','3d','5d']:
             units = {f'{time_res}': 'mm/d'}
        newDataset.attrs['units'] = units[f'{time_res}']
        ### Check if units are correct by checking one instance...not the best
        print(np.nanmean(newDataset['CH'][0][0].values))
        if (np.nanmean(newDataset['CH'][0][0].values) > 10000):
            breakpoint()
            # newDataset = newDataset / 3600
        elif np.nanmean(newDataset['CH'][0][0].values) < 0.0001:
            breakpoint()
            # newDataset = newDataset * 3600
        # breakpoint()
        # filenmae_new = path_data + f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc'
        filenmae_new = path_data + f'/{cpm}_{time_res}_RP_prmax_{season}.nc'
        newDataset.to_netcdf(path=filenmae_new)
    
    # '''-----------------------------------------------------------'''
    # ## Quick plot to check if putting Dataset together worked
    # '''-----------------------------------------------------------'''
    ### Most models are in rotated lat lon grid
    # rlat,rlon,pole = MyDataset.rlat, MyDataset.rlon, MyDataset.rotated_pole
    # try:
    #     if hasattr(pole,'grid_north_pole_longitude'):
    #         pole_lon = pole.attrs['grid_north_pole_longitude']
    #     if hasattr(pole,'grid_north_pole_latitude'):
    #         pole_lat = pole.attrs['grid_north_pole_latitude']
    # except:
    #     print('Unexpected error:', sys.exc_info()[0])
    #     raise
    # crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

    # # map_ext = np.array([-180, 180, -90, 90])  # [degree]
    # # map_ext = np.array([-0, 11, 45, 49])  # [degree]
    # map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
    # rad_earth = 6371.0  # approximate radius of Earth [km]
    # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
    #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
    # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

    # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
    #                     subplot_kw={'projection':ccrs.PlateCarree()})

    # ax.coastlines(color='black', linewidth=0.5)
    # ax.add_feature(cfeature.BORDERS, edgecolor= 'black',linewidth = 0.5)
    # ax.set_aspect("auto")
    # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    # # colors = ['#a8415b','#6a2c5a','#2c0246','#0a1f96',
    # #             '#05508c','#0570b0','#3296b4','#32a696',
    # #             '#53bd9f','#99f0b2','#cdffcd','#edfac2',
    # #             '#ffeda0','#fed976','#feb24c']
    # # reversed_colors = colors.reverse()
    # # breakpoint()
    # cmap = mpl.colors.ListedColormap(['#feb24c', '#fed976', '#ffeda0',
    #                                     '#edfac2', '#cdffcd', '#99f0b2',
    #                                       '#53bd9f', '#32a696', '#3296b4',
    #                                         '#0570b0', '#05508c', '#0a1f96',
    #                                           '#2c0246', '#6a2c5a', '#a8415b'])
    
    # for p_i, p_reg in enumerate(region_list):
    #     if p_reg == 'CH':
    #         continue
    #     if p_i == 1:
    #         mymap = MyDataset[p_reg][my_rp_ind][1].values
    #     else:
    #         mymap = np.where(np.isnan(mymap) == True,
    #                           MyDataset[p_reg][my_rp_ind][1].values, mymap)
    # # levels = np.linspace(0,110,15)
    # levels = [0,9,11,13,16,20,24,29,36,44,53,65,79,96,117]
    # col = ax.contourf(rlon, rlat, mymap, levels=levels,
    #             cmap=cmap, transform=crs, extend='max')
    

    # cbar_ax= fig.add_axes([0.92,0.122,0.02,.75])
    # cbar = plt.colorbar(col, cax=cbar_ax, label="Returnvalue [mm/h]",
    #                     orientation="vertical", format='%i')
    # # cbar.set_ticklabels(return_periods)
    # cbar.ax.tick_params(labelsize=8)
    # plt.savefig(os.path.join(plot_path, f'{cpm}_RP{my_rp}_{season}.pdf'),
    #                 format='pdf',bbox_inches='tight')
    
    # plt.show()
    # breakpoint()
    # break