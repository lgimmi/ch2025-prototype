import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import genextreme

# Input parameters
# scale_param = float(input("Enter the scale parameter: "))
# location_param = float(input("Enter the location parameter: "))
# shape_param = float(input("Enter the shape parameter: "))
scale_param = 1.25022063
location_param = 4.44584522
shape_param = 0.1024654

# Generate data from the GEV distribution
data = genextreme.rvs(shape_param, loc=location_param, scale=scale_param, size=1000)

# Plot the probability density function (PDF) of the GEV distribution
x = np.linspace(genextreme.ppf(0.001, shape_param), genextreme.ppf(0.999, shape_param), 1000)
pdf = genextreme.pdf(x, shape_param, loc=location_param, scale=scale_param)

plt.plot(x, pdf, label='PDF', color='blue')
plt.title('Generalized Extreme Value (GEV) Distribution')
plt.xlabel('X')
plt.ylabel('PDF')
plt.legend()
plt.grid(True)
plt.show()