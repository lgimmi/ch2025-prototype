#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
April 2024

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different stations and apply clausius clapeyron scaling

"""
import os
# import csv
import string
import numpy as np
# import xarray as xr
import pandas as pd
# import cartopy.crs as ccrs
import matplotlib.pyplot as plt
# import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
# from matplotlib.colors import LinearSegmentedColormap
# import matplotlib.patches as mpatches
# import warnings
# warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''

DataPath = '/users/lgimmi/MeteoSwiss/data/'
plot_path = f'/users/lgimmi/MeteoSwiss/figures/paper/scaling/'
Station_names = ['Genève - Cointrin','Zurich - Fluntern', 'Säntis',
                 'Locarno - Monti']
File_names = ['Geneva_cointrin','Zurich_fluntern','Saentis','Locarno_monti']
Indexes = ['Wiederkehrperiode [Jahre]','Wiederkehrwert [mm/Stunde]','2.5%',
           '16%','84%','97.5%']

alphabet = list(string.ascii_lowercase)

fig = plt.figure(figsize=(6, 5.4))
gs = fig.add_gridspec(nrows=2, ncols=2)
row, col, bet = 0, 0, 0
for i_station, station in enumerate(Station_names):
    csvFile = pd.read_csv(f'{DataPath}{File_names[i_station]}.csv',
                          sep=';',skiprows=10,header=0)
    return_periods = csvFile['Wiederkehrperiode [Jahre]']
    return_levels = csvFile['Wiederkehrwert [mm/Stunde]']
    
    ax = fig.add_subplot(gs[row,col])
    ax.plot(return_periods, return_levels,
             label='Best Estimate', color='black')
    # ax.plot(return_periods, csvFile['16%'],
    #          label='68%-CI', color='blue', linestyle='--')
    # ax.plot(return_periods, csvFile['84%'],
    #          color='blue', linestyle='--')
    ax.plot(return_periods, csvFile['2.5%'], linewidth=0.9,
             label='95%-CI', color='black', linestyle='--',zorder=100)
    ax.plot(return_periods, csvFile['97.5%'],linewidth=0.9,
             color='black', linestyle='--',zorder=100)
    
    ### Scaling CC (7° per °) for 1°, 2° and 4° C
    ax.plot(return_periods, return_levels + 1 * (return_levels * 0.07) ,
             label='+1°C', color='#FFD800')
    ax.plot(return_periods, return_levels + 2 * (return_levels * 0.07) ,
             label='+2°C', color='#FFB700')
    ax.plot(return_periods, return_levels + 4 * (return_levels * 0.07) ,
             label='+4°C', color='#FF7800')
    
    if station == 'Locarno - Monti':
        max_y = 156
        min_y = 26
    elif station == 'Zurich - Fluntern':
        max_y = 103
        min_y = 13
    elif station == 'Genève - Cointrin':
        max_y = 63
        min_y = 13
    elif station == 'Säntis':
        max_y = 116
        min_y = 16
    
    major_ticks_y = np.arange(min_y, max_y+1, (max_y-min_y)/10*2)
    minor_ticks_y = np.arange(min_y, max_y+1, (max_y-min_y)/10)
    ax.set_yticks(major_ticks_y, fontsize=9)
    ax.set_yticks(minor_ticks_y, minor=True, fontsize=8)


    if row == 1:
        ax.set_xlabel('Returnperiod [yr]',fontsize=13)
    if col == 0:
        ax.set_ylabel('Intensity [mm/h]',fontsize=13)
    ax.set_title(f'{station}', x=0.02, fontsize=13, loc='left')
    ax.set_title(f'{alphabet[bet]}',fontsize=12, loc='right',x=0.98)  

    ax.set_xscale('log')
    ax.set_xticks([2,5,10,20,50,100,300], fontsize=8)
    ax.get_xaxis().set_major_formatter(ScalarFormatter())
    ax.grid(which='both', axis='y', alpha=0.3) 
    plt.xlim(return_periods[0],return_periods[99])
    ax.set_ylim(min_y,max_y)

    if col == 1:
        col = 0
        row += 1
    else:
        col += 1
    bet += 1

handles, labels = plt.gca().get_legend_handles_labels()
order = [0,1,2,3,4]
fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.91, 0.02),frameon=False, fontsize=8, ncols=5)
plt.suptitle(f'Example: Temperature scaling of \n yearly station measurements (1982 - 2022)',
                      y=1.04, fontsize=16)
plt.subplots_adjust( wspace=0.35, hspace=0.40)
plt.savefig(os.path.join(plot_path, f'Station_scaling_example.pdf'),
                        format='pdf',bbox_inches='tight')