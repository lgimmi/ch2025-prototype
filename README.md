# Change in precipitation extremes – Pipeline and product prototype

<p> The goal of this project is to create a product prototype for Intesity changes of different return periods (RT) of precipitation extremes using convection-permitting climate simulations (CPMs).
</p>

## Install Python-environment 

If you are working at CSCS (Daint,Tsa,..) use the following commands to install Miniconda (Python) in your home folder `/users/<YourShortname>`. Check [Miniconda](https://docs.anaconda.com/free/miniconda/) for additional information or if you are using other operating systems and/or other machines.

```sh
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
```
and initialze Miniconda (should be bash, if not working try zsh)
```sh
~/miniconda3/bin/conda init bash
```

<br />

After successfully installing Miniconda, download/pull `alps.yml` and `ch2025.yml` and copy them to to your home folder `/users/<YourShortname>` :

Create both environments from the respective `.yml` files:
```sh
conda env create -f <EnvironmentName>.yml
```
(De)activate environments using
```sh
conda deactivate
conda activate <EnvironmentName>
```

Now you are set to run python scripts.

<br>

## How to run python scripts

There are two ways how to run python scripts:

### SBATCH on CSCS (recommended)

On CSCS you can use slurm batch systems to schedule your task to run on a HPC cluster. The shell file `send_python_scripts.sh` should function as an example. Make changes where needed and place it in queue using
```sh
sbatch send_python_scripts.sh
```
Your python file should now be waiting in queue. You can check this using
```sh
squeue -u <YourShortname>
```
For more information please consider the [CSCS webpage](https://user.cscs.ch/access/running/) on running jobs.

### On login-node / local machine

If you do not have acces to CSCS or for smaller scripts on CSCS you can run them directly in your terminal. The following command should do the trick:
```sh
python <YourPythonScript.py>
```

<br>

## Code structure (data processing and plotting): 

![Alt text](prototype/CodeStructure2.png "Flowchart of Code Structure")

<br>

## Data Processing

### Climate model data

The goal is to get extreme value estimations out of climate model output. Endproduct is a netCDF-file. For this the following python scripts are needed, starting from **Precipitation data (CPM)** in the flowchart: 
<details>     
  <summary><i>Seasons_calc_maxima.py</i></summary>

  _Use the `alps` environment to run the script._  <br> 
  The method used for this is described in detail in [Ban et al. 2018](https://link.springer.com/article/10.1007/s00382-018-4339-4) but is shortly summarized here. <br> <!-- OR use two spaces + retrun to have new line-->
  For each grid cell of individual models, the three maximum precipitation values are extracted for each season (block) of every year while independence, i.e. maxima can not be within two days of each other, is ensured. In simpler terms, the three strongest, independent events per season of each year are extracted. The CPMs which we used have ten year data slices which gives us 10x3 = 30 maxima for every block. This sample size is increased by a bootstrapping approach with the next python script before we start to estimate GEV parameters.
  

</details>

<details>     
  <summary><i>Bootstrapping.py</i></summary>

  _Use the `ch2025` environment to run the script._  <br> 
  Before estimating GEV parameters the sample size is increased by randomly subsampling the 30 block maxima we already have. Then, the R-package [gevXgpd](https://github.com/C2SM/gevXgpd) is used to estimate GEV parameters and return levels for different return periods.

</details>

<details>     
  <summary><i>Rearange_reduce.py</i></summary>

  _Use the `ch2025` environment to run the script._  <br> 
  Brings data into a better form to be stored as netCDF-file in the final script.

</details>

<details>    
  <summary><i>Save_as_ncfile.py</i></summary>

  _Use the `alps` environment to run the script._  <br> 
  Stores estimated return levels for different seasons and regions into netCDF-file.

</details>


### Station data:

Same principle as for model data but different scripts.

<details>     
  <summary><i>stations_maxima.py</i></summary>

  _Use the `ch2025` environment to run the script._  <br> 
  Combination of the scripts for climate model output. Gives you .npy-files

</details>

<details>     
  <summary><i>assign_Stations_to_region.py</i></summary>

  _Use the `alps` environment to run the script._  <br> 
  Assigns individual stations to different regions of Switzerland. Used to plot maps correctly and use correct stations when only a single region is of interest.

</details>

<br>

 ## General information

 With the scripts described above you got netCDF-files witzh which you can continue to work however you like. The code structure and this git provides you with some additional scripts which can be used to create ensemble analysis (Calc_ensemble_ncfile.py and Calc_ensemble_scaling.py) or provide you with some plots for analysis. 

<br>

## Figures: 

Some example of figures are shown below. 


### Evaluation

![Alt text](prototype/GEV_JJA_CH.png "Output from plot_GEV.py")
Summer-time (JJA) hourly extreme precipitation return levels for a set of ten convection-permitting climate models (panels a-j). For validation, each panel also illustrates observational estimates expressed as the mean across 86 Swiss station observations with black solid lines indicating the best-estimate for a return level and dashed lines upper and lower uncertainty ranges. In each panel, individual CPM simulations are depicted in light blue, with the solid line representing the best-estimate and shading illustrating the uncertainty range. If available (at time of analysis), the corresponding driving 12km RCM is given in red. CPM and RCM simulations are driven by ERA-Interim reanalysis for period 2000-2009.

# Product Prototypes


![Alt text](prototype/Eval_Ensemble_JJA_RP2.png "Ensemble mean Map values with individual Station measurements")
Summer-time (JJA) hourly extreme precipitation maps for a two-year return level for an CPM ensemble. Individual dots denote different station measurements. This map is mimicking [Meteosuisse](https://www.meteoswiss.admin.ch/services-and-publications/applications/ext/climate-extremes-maps.html) return level plots. 

<br>

![Alt text](prototype/IDFs_CH_JJA.png "Intesity-Duration-Frequency curves for ensemble of CPMs")
Intesity-Duration-Frequency plot for different Return periods (colors, dots) for an ensemble analysis and station observations (colors, x). The left panels show classic IDFs for todays climate for time granularities ranging from 1 hour to 5 days. The right panels show relative and absolute changes of different time granularities until the end of century in a high emission scenario (RCP8.5)

<br>

![Alt text](prototype/Station_scaling_example.png "Applied scaling to different station measurements")
Prototype application of a Clausius-Clapeyron-based temperature-scaling for individual Swiss station observations (Zurich, Geneva, Saentis and Locarno)in period 1982-2022 (yearly) as distributed via the climate-extremes.ch platform of the Swiss Federal Office of Meteorology and Climatology MeteoSwiss. Panels show extreme precipitation return levels as a function of return periods (years) for observations (black) and corresponding uncertainties (dashed) as well as different warming-dependent representations of potential future return levels denoted in orange and red color.