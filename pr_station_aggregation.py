# -*- coding: utf-8 -*-
"""
December 2023

Authors:
- Leandro Gimmi

Description:
Aggregation of observational station data

"""
import pandas as pd

### Read CSV file
fpath_hour = '/users/lgimmi/MeteoSwiss/data/rre150h0_1995_2010_allstats_andpartnerstats_withNA.csv'
fpath_day = '/users/lgimmi/MeteoSwiss/data/rre150d0_1995_2010_allstatswithpartners_allNA.csv'
hourdata = pd.read_csv(fpath_hour)
daydata = pd.read_csv(fpath_day)
# breakpoint()

### Convert 'time' column to datetime format
hourdata['time'] = pd.to_datetime(hourdata['time'], format='%Y%m%d%H%M')
daydata['time'] = pd.to_datetime(daydata['time'], format='%Y%m%d%H%M')

### Set 'time' column as the index
hourdata.set_index('time', inplace=True)
daydata.set_index('time', inplace=True)

### Resample and aggregate hourly dtaa to three and six-hourly 
three_hourly_data = hourdata.resample('3H').sum()
six_hourly_data = hourdata.resample('6H').sum()
three_day_data = daydata.resample('3D').sum()
five_day_data = daydata.resample('5D').sum()


### Resetting index 
three_hourly_data.reset_index(inplace=True)
six_hourly_data.reset_index(inplace=True)
three_day_data.reset_index(inplace=True)
five_day_data.reset_index(inplace=True)

### Save aggregated hourdata to new CSV files
three_hourly_data.to_csv('/users/lgimmi/MeteoSwiss/data/three_hourly_aggregated.csv', index=False)
six_hourly_data.to_csv('/users/lgimmi/MeteoSwiss/data/six_hourly_aggregated.csv', index=False)
three_day_data.to_csv('/users/lgimmi/MeteoSwiss/data/three_day_aggregated.csv', index=False)
five_day_data.to_csv('/users/lgimmi/MeteoSwiss/data/five_day_aggregated.csv', index=False)