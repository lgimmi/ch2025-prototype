#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
August 2023

Authors:
- Leandro Gimmi

Description:
Extract three independent precipitation maxima for individual
 CORDEX-FPS models and regions. Dry areas (>15 maxima are <1mm/h) are excluded.
 This method follows Ban et al. 2018 (DOI: 10.1007/s00382-018-4339-4)
 Uses gridded model data stored as ncfile as input, returns .npy file

"""
import os
import glob
import time
import warnings
import numpy as np
import xarray as xr 
import cartopy.crs as ccrs
from natsort import natsorted
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature


'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical / rcp85_moc /rcp85_eoc
scenario = 'historical'

### xHourly, xDaily
time_res = '3hr'

### Seasons: DJF, MAM, JJA, SON, Year
season = 'Year'

### CH2018 regions: CHNE, CHW, CHS ,CHAE or CHAW
regions = ['CHNE', 'CHW', 'CHS', 'CHAE', 'CHAW']

'''-------------------------------------------------------------------------'''
### ------------------------------- CPM ----------------------------------- ###
path_prdata = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr/{scenario}'
path_obs = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr/obs/*'
path_regions = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
plot_path = '/users/lgimmi/MeteoSwiss/figures'
path_save = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_maxima/'
             +f'{scenario}')

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15',
        'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME',
        'HadREM3-RA-UM10.1','RegCM4-7']
# Institutes = ['MOHC']
# CPMs = ['HadREM3-RA-UM10.1']
rcm = None

### ------------------------------- RCM ----------------------------------- ###
# path_prdata = f'/scratch/snx3000/lgimmi/store/rcm/1hr/pr'
# # path_obs = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr/obs/*'
# path_regions = '/scratch/snx3000/lgimmi/store/rcm/masks'
# plot_path = '/users/lgimmi/MeteoSwiss/figures'
# path_save = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_maxima/'
#              +f'{scenario}')

# Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
# CPMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
#         'RegCM4-7']
# rcm = 1

'''-------------------------------------------------------------------------'''
if scenario == 'evaluation':
    Institutes.insert(0,'obs')
    CPMs.insert(0,'obs')


for i_region, region in enumerate(regions):
    i_region += 1

    if scenario == 'evaluation':
        start_year = 2000
    elif scenario == 'historical':
        start_year = 1996
    elif scenario == 'rcp85_moc':
        start_year = 2040
        path_prdata = path_prdata[:-4] + '/moc'
        path_save = path_save[:-4] + '/moc'
    elif scenario == 'rcp85_eoc':
        start_year = 2090
        path_prdata = path_prdata[:-4] + '/eoc'
        path_save = path_save[:-4] + '/eoc'

    for i_cpm, cpm in enumerate(Institutes):
        if cpm == 'obs':
            continue
        print((f'Scenario: {scenario}, Region: {region}, Model: {cpm}, '
                + f'Time res: {time_res}, Season: {season}').center(50,'-'))
        
        maxima_ten_year = []
        check_compare_outofbounds_gridcell = False
        
        ### Get filenames
        if cpm == 'KNMI':
            filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
        elif cpm == 'MOHC':
            filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
        elif cpm == 'CLMcom-KIT' and scenario == 'evaluation':
            filename = (path_prdata + '/' + f'*{cpm}**CCLM5-0-14*')
        else:
            filename = (path_prdata + '/' + f'*{cpm}**{CPMs[i_cpm]}*')
        filenames_list = natsorted(glob.glob(filename))

        range_num = 10
        ### ETH RCM only has data from 2000
        if rcm == 1 and cpm == 'CLMcom-ETH':
            range_num = 9

        for i_year in range(range_num):
            ### Load correct files (dependent on year, generally -1,i_year,+1)
            if i_year == 0:
                if cpm == 'MOHC':
                    filenames = filenames_list[:24]
                else:
                    filenames = filenames_list[:i_year+2]

            elif i_year == 9:
                if cpm == 'MOHC':
                    filenames = filenames_list[-24:]
                else:
                    filenames = filenames_list[i_year-1:]
            else:
                if cpm == 'MOHC':
                    filenames = filenames_list[12*i_year-12:24+12*i_year]
                else:
                    filenames = filenames_list[i_year-1:i_year+2]

            print((f'Year: {2000+i_year}').center(50,' '))
            warnings.simplefilter("ignore")
            ds = xr.open_mfdataset(filenames, use_cftime=True)
            da = ds['pr']
            ds.close()

            ### open mask file for regions
            if cpm == 'MOHC':
                region_file = (path_regions + f'/*{CPMs[i_cpm]}*Ch2018*')
            else:
                region_file = (path_regions + f'/*{cpm}*{CPMs[i_cpm]}*Ch2018*')
            ds_region = xr.open_mfdataset(region_file)
            mask_region = ds_region['orog']
            ds_region.close()

            ### Allign masks to dimensions and cooridinates of CPM as they
            ###  differ slightly on the 7th or higher decimal place
            mask_region = mask_region.expand_dims(dim={"time": len(da.time)})
            mask_region = mask_region.assign_coords(time=da.time)
            da, mask_region = xr.align(da, mask_region, join="override")

            ### '''------------------------------------------------------'''
            # ### Diff. way to mask data (extent outside region is kept)
            # ### Separate masking into blocks of size limited to 1.0GB
            # ### to avoid freezing of code due to memory issues
            # ### Used for CMCC model as it has coordinate problem
            # '''--------------------------------------------------------'''

            def masking(xarray_dataarray,  mask, max_block = 1.0):
                '''
                Separate masking into blocks of size limited to 1.0GB 
                (max_block) to avoid freezing of code due to memory issues

                -----
                Returns maked input array as numpy array

                '''
                global i_region

                block_size = max_block
                da = xarray_dataarray
                mask_region = mask
                block_size = 1.0

                len_time = da['time'].size
                if ('x' in list(da.coords)) and ('y' in list(da.coords)):
                    len_x = da['x'].size
                    len_y = da['y'].size
                    out_dim = {'y','x'}
                elif ('rlon' in list(da.coords)) and ('rlat' in list(da.coords)):
                    len_x = da['rlon'].size
                    len_y = da['rlat'].size
                    out_dim = {'rlat','rlon'}
                elif ('lon' in list(da.coords)) and ('lat' in list(da.coords)):
                    len_x = da['lat'].size
                    len_y = da['lon'].size
                    out_dim = {'lon','lat'}
                else:
                    print(f'The model {cpm} uses an unknown coordinate systems'
                        +f' -- {list(da.coords)} -- ')
                if cpm == 'CLMcom-CMCC':
                    len_x = ds['rlon'].size
                    len_y = ds['rlat'].size
                    out_dim = {'rlat','rlon'}

                dataarray = np.empty((len_time, len_y, len_x), dtype=np.float32)
                dataarray.fill(np.nan)
                maskarray = np.empty((len_time, len_y, len_x), dtype=np.float32)
                maskarray.fill(np.nan)
                da_masked = np.empty((len_time, len_y, len_x), dtype=np.float32)
                da_masked.fill(np.nan)

                num_blocks = int(np.ceil((da_masked.nbytes/ (10 ** 9))/block_size))
                lim = np.linspace(0, len_time, num_blocks + 1, dtype=np.int32)  
                
                for i in range(num_blocks):
                    t_beg = time.time()
                    slice_t = slice(lim[i], lim[i + 1])
                    dataarray[slice_t,:,:] = da[slice_t,:,:].values
                    maskarray[slice_t,:,:] = mask_region[slice_t,:,:].values

                    da_masked[slice_t,:,:] = np.where(
                        maskarray[slice_t,:,:] == float(i_region),
                        dataarray[slice_t,:,:] * multiply, #change units to mm/hr
                        0.0
                        )
                    
                    print('Data blocks loaded: '+str(i + 1)+'/'+str(num_blocks))
                    print('Time for masking for this block: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')
                return da_masked


            # '''-------------------------------------------------------'''
            ### Masking using xarray can drop unnecessairy grid cells to 
            ### speed up code and free up memory (Not working with CMCC)
            # '''-------------------------------------------------------'''
            try: # check units
                if da.units != 'kg m-2 s-1': 
                    raise RuntimeError
            except AttributeError or RuntimeError:
                print(f'Error with {cpm} - make sure to check its units')
            multiply = 3600
            maxima_threshold = 250
            multiply = 1
            if da.units == 'kg m-2 s-1':
                multiply = 3600
            
            if cpm != 'CLMcom-CMCC':
                t_beg = time.time()
                xrda_masked = da.where(mask_region == i_region,drop = True)
                da_masked = np.nan_to_num(xrda_masked.values) * multiply
                print('Time for masking and changing units: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')
            else:
                t_beg = time.time()
                da_masked = masking(da, mask_region)
                print('Time for masking and changing units: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')

            da_masked = np.where(da_masked >= maxima_threshold, np.nan,
                                    da_masked)
           
            # '''-----------------------------------------------------------'''
            ### Quick plot to check if it masking worked
            # '''-----------------------------------------------------------'''

            # mask_check = np.empty((len_time, len_y, len_x), dtype=np.float32)
            # mask_check.fill(np.nan)
            # mask_check[0,:,:] = np.where(
            #         maskarray[0,:,:] == float(i_region),
            #         dataarray[0,:,:]*3600, #change units to mm/hr
            #         np.nan)
            # # map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
            # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
            # rad_earth = 6371.0  # approximate radius of Earth [km]
            # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

            # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
            #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # ax.coastlines(color='grey', linewidth=0.5)
            # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
            # ax.set_aspect("auto")
            # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.contourf(mask_region.lon, mask_region.lat, mask_check[0], 
            #     cmap='terrain', transform= ccrs.PlateCarree())
            # plt.savefig(os.path.join(plot_path, 'masktest.pdf'),
            #                         format='pdf',bbox_inches='tight')            


            # def sel_current(da, mask_region):
            #     current_year = da.sel(time=da.time.dt.year.isin(i_year+2000))
            #     current_mask = mask_region.sel(
            #         time=da.time.dt.year.isin(i_year+2000))
            #     return current_year, current_mask
            
            # def sel_next(da, mask_region):
            #     year = da.sel(time=da.time.dt.year.isin(i_year+2000+1))
            #     mask = mask_region.sel(
            #         time=da.time.dt.year.isin(i_year+2000+1))
            #     return year, mask
            
            # def sel_prev(da, mask_region):
            #     year = da.sel(time=da.time.dt.year.isin(i_year+2000-1))
            #     mask = mask_region.sel(
            #         time=da.time.dt.year.isin(i_year+2000-1))
            #     return year, mask
            
            # if i_year == 0:
            #     current_year, current_mask = sel_current(da,mask_region)
            #     next_year, next_mask = sel_next(da, mask_region)
            #     da_masked_current = masking(current_year, current_mask)
            #     da_masked_next = masking(next_year, next_mask)

            # elif i_year == 9:
            #     current_year, current_mask = sel_current(da,mask_region)
            #     prev_year, prev_mask = sel_prev(da, mask_region)
            #     da_masked_current = masking(current_year, current_mask)
            #     da_masked_prev = masking(prev_year, prev_mask)

            # else:
            #     prev_year, prev_mask = sel_prev(da, mask_region)
            #     current_year, current_mask = sel_current(da,mask_region)
            #     next_year, next_mask = sel_next(da, mask_region)
            #     da_masked_current = masking(current_year, current_mask)
            #     da_masked_next = masking(next_year, next_mask)
            #     da_masked_prev = masking(prev_year, prev_mask)
            # current_year, current_mask = sel_current(da,mask_region)
            # da_masked_current = masking(current_year, current_mask)

            # '''-----------------------------------------------------------'''
            ### Calculate maxima of each gridcell of one year. Ensure 
            ### independence by removing 2 days worth of timesteps before and
            ### after maxima
            # '''-----------------------------------------------------------'''
            maxima_num = 1
            # breakpoint()

            def independent_maxima(unsliced_array):
                '''
                Returns maxima of time axis of input 3D-array and changes input
                array to ensure indpendence for next maxima (removes maxima
                that were found + two days before and after)
                ''' 
                t_beg = time.time()
            
                if time_res == '1hr':
                    twoday = 48
                    daily_timesteps = 24

                ### Choose current year for calculations. Keep others for 
                if i_year == 0:
                    if (start_year+i_year)%4 == 0:
                        ### Leap year
                        array = unsliced_array[:(daily_timesteps*366)]
                        array2day = unsliced_array[
                            :(daily_timesteps*366+twoday)]
                    else:
                        array = unsliced_array[:(daily_timesteps*365)]
                        array2day = unsliced_array[
                            :(daily_timesteps*365+twoday)]

                elif i_year == (range_num-1):
                    if (start_year+i_year)%4 == 0:
                        ### Leap year
                        array = unsliced_array[-(daily_timesteps*366):]
                        array2day = unsliced_array[
                            -(daily_timesteps*366-twoday):]
                    else:
                        array = unsliced_array[-(daily_timesteps*365):]
                        array2day = unsliced_array[
                            -(daily_timesteps*365-twoday):]
                
                else:
                    if (start_year+i_year)%4 in [0,2]:
                        ### Leap year
                        array = unsliced_array[
                            (daily_timesteps*365):-(daily_timesteps*365)]
                        array2day = unsliced_array[(daily_timesteps*365-twoday)
                                                :-(daily_timesteps*365-twoday)]
                    elif (start_year+i_year)%4 == 1:
                        array = unsliced_array[
                            (daily_timesteps*366):-(daily_timesteps*365)]
                        array2day = unsliced_array[(daily_timesteps*366-twoday)
                                                :-(daily_timesteps*365-twoday)]
                    elif (start_year+i_year)%4 == 3:
                        array = unsliced_array[
                            (daily_timesteps*365):-(daily_timesteps*366)]
                        array2day = unsliced_array[(daily_timesteps*365-twoday)
                                                :-(daily_timesteps*366-twoday)]

                ### Get index of maxima for this year + for overlap
                try:
                    ind_array = np.nanargmax(array, axis = 0, keepdims = True)
                except ValueError:
                    breakpoint()
                try:
                    ind_array2day = np.nanargmax(array,axis=0,keepdims = True)
                except ValueError:
                    breakpoint()

                ### Find maxima and save values
                max_array = np.nanmax(array, axis = 0, keepdims = True)
                global maxima_ten_year
                maxima_ten_year.append(max_array)

                # ### Get index of maxima for independent check
                
                
                # # array_copy = np.copy(array) ?????????

                # global check_ignore_outofbounds_gridcell
                # check_ignore_outofbounds_gridcell = False

                # i_func_it = {}

                ### Exclude maxima and values two days before and after max value
                for i_erase in range(twoday):

                    breakpoint()

                    # ### Replace value of specific gridcell that previously reached 
                    # ### end of year at independence check (otherwise would again
                    # ### reach end of year and trigger IndexError)
                    # if check_ignore_outofbounds_gridcell:
                    #     for key in i_func_it.keys():
                    #         for i_gridcell in range(
                    #             len(i_func_it[key][0][0])):
                    #             (ind_array[0][i_func_it[key][0][1][i_gridcell]]
                    #             [i_func_it[key][0][2][i_gridcell]]) = \
                    #             len(current_year.time) - i_erase - 1

                    # ### Exclude maxima + values two days later
                    # try:
                    #     np.put_along_axis(array, ind_array + i_erase, 
                    #                     0.0, axis = 0)
                    # except IndexError:
                    #     ### -------------------------------------------------------
                    #     ### Values that should be excluded are in the next year
                    #     ### -------------------------------------------------------
                    #     global maxima_num
                    #     print(f'Model: {cpm}, year: {i_year}, maxima {maxima_num}'
                    #         +' \n Values that should be excluded are in the next'
                    #         + ' year')

                    #     ### save grid cell(s) location and steps of the two days 
                    #     global next_year_changes
                    #     if i_erase in next_year_changes.keys():
                    #         next_year_changes[i_erase].extend([np.where(
                    #             ind_array + i_erase == len(current_year.time))])
                    #     else:
                    #         next_year_changes[i_erase] = [np.where(
                    #             ind_array + i_erase == len(current_year.time))]
                        
                    #     # save maxima value of that grid cell(s) and timestep
                    #     x = next_year_changes[i_erase][0][1][0]
                    #     y = next_year_changes[i_erase][0][2][0]
                    #     timestep = ind_array[0][x][y]

                    #     i_func_it[i_erase] = [np.where(
                    #             ind_array + i_erase == len(current_year.time))]
                        
                    #     ### Set check marks to True to ignore grid cell(s) in the
                    #     ### next iteration 
                    #     check_ignore_outofbounds_gridcell = True
                    #     global check_compare_outofbounds_gridcell
                    #     check_compare_outofbounds_gridcell = True

                    # ### Exclude maxima + values two days before
                    # try: 
                    #     np.put_along_axis(array,ind_array - i_erase,
                    #                     0.0, axis = 0)
                    # except IndexError:
                    #     ### -------------------------------------------------------
                    #     ### Values that should be excluded are in the previous year
                    #     ### -------------------------------------------------------
                    #     print(f'Model: {cpm}, year: {i_year}, maxima {maxima_num}'
                    #         +' \n Values that should be excluded are in the '
                    #         + 'previous year')
                        
                        # ### save grid cell(s) location and steps of the two days 
                        # global next_year_changes
                        # if i_erase in next_year_changes.keys():
                        #     next_year_changes[i_erase].extend([np.where(
                        #         ind_array + i_erase == len(da.time))])
                        # else:
                        #     next_year_changes[i_erase] = [np.where(
                        #         ind_array + i_erase == len(da.time))]
                        
                        # # save maxima value of that grid cell(s) and timestep
                        # x = next_year_changes[i_erase][0][1][0]
                        # y = next_year_changes[i_erase][0][2][0]
                        # timestep = ind_array[0][x][y]

                        # i_func_it[i_erase] = [np.where(
                        #         ind_array + i_erase == len(da.time))]
                        
                        # ### Set check marks to True to ignore grid cell(s) in the
                        # ### next iteration 
                        # check_ignore_outofbounds_gridcell = True
                        # global check_compare_outofbounds_gridcell
                        # check_compare_outofbounds_gridcell = True

                print(f'Time for calculating maxima {maxima_num}: ' + 
                    '%.2f' % (time.time() - t_beg) + ' s')
                maxima_num+=1

                return array, max_array
            

            ### -------------------------------------------------------------------
            ### -------------------------------------------------------------------
            
            def independent_maxima_ignore_outofbounds(array):
                '''
                Only for use within outofbounds check, used for comparison of max.
                Returns maxima of time axis of input 3D-array and changes input
                array to ensure indpendence for next maxima (removes maxima
                that were found + two days before and after).

                '''      
                if time_res == '1hr':
                    twoday = 48
                ### Get maxima and save values
                max_array = np.nanmax(array, axis = 0, keepdims = True)
                global maxima_ten_year
                maxima_ten_year.append(max_array)
                ### Get index of maxima for independent check
                ind_array = np.nanargmax(array, axis = 0, keepdims = True)
                ### Exclude maxima and values two days before and after max value
                for i_erase in range(twoday+1):
                    ### Exclude maxima + values two days later
                    try:
                        np.put_along_axis(array, ind_array + i_erase, 
                                        0.0, axis = 0)
                    except IndexError:
                        continue
                    ### Exclude maxima + values two days before
                    try: 
                        np.put_along_axis(array,ind_array - i_erase,
                                        0.0, axis = 0)
                    except IndexError:
                        continue
                return array, max_array


            ### -------------------------------------------------------------------     
            ### Exclude values that are conflicting with independence statement
            ### from year before. But first check if deleted values won't be a 
            ### higher maxima -> then this one should be chosen and not the one
            ### a year before
            def compare_maxima_following_year():
                global next_year_changes
                next_year, next_mask = sel_next(da, mask_region)
                da_masked_next = masking(next_year, next_mask)

                return None

            ### Outside of function as overlapping into other years can happen 
            ### for every maxima iteration
            next_year_changes = {}
            
            maxima_num = 1
            
            array_loop1, maxima_1 = independent_maxima(da_masked)

            if check_compare_outofbounds_gridcell:
                compare_maxima_following_year()

            array_loop2, maxima_2 = independent_maxima(array_loop1)
            array_loop3, maxima_3 = independent_maxima(array_loop2)

        # '''-----------------------------------------------------------'''
        ### Masking dry areas (>15 maxima with <1mm/h)
        # '''-----------------------------------------------------------'''
        maxima_ten_year = np.squeeze(np.array(maxima_ten_year))
        dry_areas = np.count_nonzero(maxima_ten_year < 1, axis = 0)
        maxima_no_dry = np.where(dry_areas > 15, np.nan, maxima_ten_year)

        try:
            rm_old_file = 'rm ' + path_save + '/' + f'maxima_{cpm}_{region}.npy'
            os.system(rm_old_file)
        except:
            continue

        np.save(path_save + '/' + f'maxima_{cpm}_{region}.npy',
                    np.array(maxima_ten_year, dtype=object), allow_pickle=True)
