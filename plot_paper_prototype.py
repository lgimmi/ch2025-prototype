#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
March 2024

Authors:
- Leandro Gimmi

Description:
Plot prototype of scaled station observation data

"""
import os
import copy
import string
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc, rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = ['1d','1hr']

### Seasons: DJF, MAM, JJA, SON
# seasons = ['JJA', 'DJF']
seasons = ['MAM','SON']

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
# rp_index = np.arange(2,201,1)

'''-------------------------------------------------------------------------'''
### ------------------------------- CPMs ----------------------------------- ###
plot_path = f'/users/lgimmi/MeteoSwiss/figures/paper/scaling/'

path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH',
              'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom', 'ensemble']

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}

alphabet = list(string.ascii_lowercase)

'''-------------------------------------------------------------------------'''
for region in region_numbers.keys():
    ### Open Figure
    fig = plt.figure(figsize=(6, 5.4))
    gs = fig.add_gridspec(nrows=2, ncols=2)
    row, column, bet = 0, 0, 0
    for season in seasons:
        for tres in time_res:
            ax = fig.add_subplot(gs[row,column])

            ### open region and station data
            path_stations = '/users/lgimmi/MeteoSwiss/data' 
            path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
            if tres == '1hr':
                fname_metadata = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                                    +'allstats_andpartnerstats_withNA.csv')
            elif tres == '1d':
                fname_metadata = (path_stations + '/station_metadat_rre150d0_2000_'
                                    +'2009_allstatswithpartnerstats_allNA.csv')
            path_data_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{tres}/'
                                +f'pr_GEV/historical')
            path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{tres}/'
                                +f'pr_GEV/{scenario[:-4]}/{scenario[-3:]}')
                
            data = pd.read_csv(fname_metadata)
            lon = data['longitude']
            lat = data['latitude']

            filename_station = (path_data_hist + f'/OneYearStep_Values_stations_{season}_'
                                +f'historical_{tres}.npy')
            filename_region = (path_region_stations + 
                            f'/stations_region_historical_{tres}.csv')
            station_data = np.load(filename_station, allow_pickle=True)
            station_region = pd.read_csv(filename_region)
            st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []

            for rp in return_periods:
                st1, st2, st3, st4 = [],[],[],[]
                for i_station, station in enumerate(station_region.keys()):
                    if station_region.values[0][i_station] != region and region != 'CH':
                        continue
                    try:
                        st1.append(station_data[station][rp][2])
                        st2.append(station_data[station][rp][3])
                        st3.append(station_data[station][rp][0])
                        st4.append(station_data[station][rp][1])
                    except:
                        continue
                st_ten_perc.append(np.nanmean(st1))
                st_ninety_perc.append(np.nanmean(st2))
                st_mean.append(np.nanmean(st3))
                st_median.append(np.nanmean(st4))

            ax.plot(return_periods, st_median, label=f'Stations',
                    color='black', linestyle='-', alpha=1,
                    linewidth=1.2,zorder=100)
            ax.plot(return_periods, st_ten_perc, label=r'10$^{th}$-90$^{th}$P',
                    color='black', linestyle='--', alpha=1,
                    linewidth=1.,zorder=-80)
            ax.plot(return_periods, st_ninety_perc,
                    color='black', linestyle='--', alpha=1,
                    linewidth=1.,zorder=-80)
            scaling_value = np.array(np.load(path_data + f'/ens_scaling_{season}_{tres}_{region}.npy',
                    allow_pickle=True))
            # scaling_value = 7

            value = np.array(st_median)
            one_degree = value + (value / 100 * scaling_value)
            two_degree = value + (value / 100 * (2*scaling_value))
            four_degree = value + (value / 100 * (4*scaling_value))
            eight_degree = value + (value / 100 * (8*scaling_value))


            ax.plot(return_periods, one_degree, label=f'+1°C',
                    color='#FFD800', linestyle='-', alpha=1,
                    linewidth=1.2,zorder=90)
            ax.plot(return_periods, two_degree, label=f'+2°C',
                    color='#FFB700', linestyle='-', alpha=1,
                    linewidth=1.2,zorder=80)
            ax.plot(return_periods, four_degree, label=f'+4°C',
                    color='#FF7800', linestyle='-', alpha=1,
                    linewidth=1.2,zorder=70)
            # ax.plot(return_periods, eight_degree, label=f'+8°',
            #         color='#FF3200', linestyle='-', alpha=1,
            #         linewidth=1.2,zorder=60)
            
            ### Plot settings
            if season == 'DJF' and tres == '1hr':
                max_y = 20
                min_y = 0
            elif season == 'DJF' and tres == '1d':
                max_y = 90
                min_y = 20
            elif season in ['JJA'] and tres == '1hr':
                max_y = 70
                min_y = 10
            elif season in ['JJA'] and tres == '1d':
                max_y = 140
                min_y = 40

            elif season == 'MAM' and tres == '1hr':
                max_y = 36
                min_y = 6
            elif season == 'MAM' and tres == '1d':
                max_y = 130
                min_y = 30
            elif season in ['SON'] and tres == '1hr':
                max_y = 26
                min_y = 6
            elif season in ['SON'] and tres == '1d':
                max_y = 145
                min_y = 35
            
            ax.set_title(f'{season_names[season]} | {tres}',fontsize=11,
                              loc='left')
            ax.set_title(f'{alphabet[bet]}',fontsize=10,
                              loc='right',x=0.98 )

            major_ticks_y = np.arange(-max_y, max_y+1, (max_y-min_y)/10*2)
            minor_ticks_y = np.arange(-max_y, max_y+1, (max_y-min_y)/10*1)
            if tres == '1hr':
                ax.set_ylabel('Intensity [mm/h]', fontsize=9,
                           labelpad=0.3)
            elif tres == '1d':
                ax.set_ylabel('Intensity [mm/d]', fontsize=9,
                           labelpad=0.3)
            ax.set_yticks(major_ticks_y)
            ax.set_yticks(minor_ticks_y, minor=True)
            ax.tick_params(axis='both', which='both', labelsize=8)
            ax.set_ylim(min_y,max_y)
            ax.grid(which='both', axis='y', alpha=0.3)
            
            ax.set_xscale('log')
            ax.set_xlabel('Returnperiod [yr]', fontsize=9)
                
            ax.set_xticks([5,10,20,50,100])
            ax.get_xaxis().set_major_formatter(ScalarFormatter())
            plt.xlim(return_periods[0],return_periods[-1])
            plt.suptitle(f'Example: Temperature scaling of seasonal \n Swiss station observations (1995-2005)' + '\n'
                          + '', y=1.015,
                          fontsize=12)

            

            if column == 1:
                column = 0
                row += 1
            else:
                column += 1
            bet+=1
    handles, labels = plt.gca().get_legend_handles_labels()
    order = [0,1,2,3,4]
    # breakpoint()
    # fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
    #         bbox_to_anchor=(0.7, 0.039),frameon=False, fontsize=8, ncols=3)
    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.89, 0.03),frameon=False, fontsize=8, ncols=5)

    plt.subplots_adjust( wspace=0.35, hspace=0.45)
    plt.savefig(os.path.join(plot_path,
                f'Paper_Prototype_Hist_{region}_{seasons[0]}{seasons[1]}.pdf'),
                format='pdf',bbox_inches='tight')
    # plt.show()
    # breakpoint()
            