#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Calculate multimodel mean from available model ensemble for every returnperiod

"""
import os
import copy
import warnings
import numpy as np
import xesmf as xe
import xarray as xr 
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''
### ensemble mean or median?
mean = True
median = True

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'historical'

### Choose between 1hr / 3hr / 6hr / 1d / 3d / 5d
time_res = '1d'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
return_periods = np.append(return_periods, [150, 200, 250, 300])
# return_periods = np.append(return_periods, [150, 200])
# return_periods = [2,5,10,20,30,50,100,200,300]
# return_periods = np.arange(2,201,1)


seasons = ['JJA', 'DJF', 'SON', 'MAM']
# seasons = ['DJF']
regions = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}
stats = ['Mean','Median','10P','90P']

'''-------------------------------------------------------------------------'''
### - - - - - - - - - - - - - - - -  CPM  - - - - - - - - - - - - - - - - - - -
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_tas = ('/scratch/snx3000/lgimmi/store/ALP-3/1d/tas/cc_signal/'
            +f'{scenario[-3:]}')

Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-9','CCLM5-0-15',
        'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME']
# if time_res == '1d' and scenario == 'evaluation':
#     Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-JLU',
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-15',
#         'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME']

rcm = None

### - - - - - - - - - - - - - - - -  RCM  - - - - - - - - - - - - - - - - - - -
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')

# if scenario == 'evaluation':
#     Institutes = ['CLMcom-ETH', 'CLMcom-BTU','KNMI','HCLIMcom']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-9','RACMO23E','HCLIM38-ALADIN']
# else:
#     Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom']
#     CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN']

# rcm = 1

'''-------------------------------------------------------------------------'''
if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'


if mean != True and median != True:
    print('Please choose either or at least one of [ mean, median ]')
    raise NameError

for i_seas, season in enumerate(seasons):
    print((season.center(60,'-')))

    ### Regridd all models first, then continue, saves alot of time (precip)
    for i_cpm, cpm in enumerate(Institutes):
        filename = path_data + f'/{cpm}_{time_res}_RP_prmax_{season}.nc'
        # filename = path_data + f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc'
        ds = xr.open_dataset(filename)
        if cpm == 'CLMcom-CMCC':
            try:
                ds = ds.rename({'x':'rlon','y':'rlat'})
            except:
                pass
        if i_cpm == 0:
            target_grid, target_name = copy.deepcopy(ds), cpm
            filename_new = (path_data + 
                f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                # f'/OneYearStep_Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                            
            try:
                rm_old_file = ('rm ' + filename_new)
                os.system(rm_old_file)
            except:
                pass
            ds.to_netcdf(path=filename_new)
        else:
            regridder = xe.Regridder(ds, target_grid, "bilinear")
            print(f'Regridding {cpm} to {target_name} for multimodel mean')
            ds_regridded = regridder(ds)
            filename_new = (path_data + 
                f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                # f'/OneYearStep_Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            try:
                rm_old_file = ('rm ' + filename_new)
                os.system(rm_old_file)
            except:
                pass
            ds_regridded.to_netcdf(path=filename_new)

        ds.close()
    
    onetime = 0
    onetime_scaling = 0
    ### Calculate multimodel mean/median for every RP, region and stat
    for region in regions.keys():
        print((region.center(50,'-')))
        for i_rp, rp in enumerate(return_periods):
            print(rp)
            
            for i_stat, stat in enumerate(stats):
                # print(stat)
                for i_cpm, cpm in enumerate(Institutes):
                    filename = (path_data + 
                    f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                    # f'/OneYearStep_Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
                    ds = xr.open_dataset(filename)

                    try:
                        da = ds[region][i_rp][i_stat]
                    except:
                        breakpoint()
                    # print(np.nanmean(da.values))
                    # if np.nanmean(da.values) > 10000:
                    #     print((f'dividing {cpm} by 3600').center(50,' '))
                    #     da = da / 3600
                    #     print((str(np.nanmean(da.values))).center(70,' '))
                    if onetime == 0:
                        if mean == True:
                            ds_copy = ds.copy(deep=True)
                        if median == True:
                            ds_copy_median = ds.copy(deep=True)
                        print(f'Copied ds, season: {season}')
                        onetime += 1
                    if (scenario in ['rcp85_moc', 'rcp85_eoc'] and
                        cpm != 'CLMcom-ETH'):
                        if onetime_scaling == 0:
                            ds_copy_scaling = ds.copy(deep=True)
                            onetime_scaling += 1
                            
                    if i_cpm == 0:
                        if mean == True:  
                            multimodel = da
                        if median == True:
                            multimodel_median = [da]
                    else:
                        if mean == True:
                            multimodel += da
                        if median == True:
                            multimodel_median.append(da)
                    ds.close()

                ### Calculate multimodel mean
                if mean == True:
                    mmm = multimodel / (len(Institutes)-1)
                ### Calculate multimodel median
                if median == True:
                    mmmedian = np.median(multimodel_median, axis=0)

                # replace all values with multimodelmean in copied ds to save
                if mean == True:
                    ds_copy[region][i_rp][i_stat] = \
                        xr.where(ds_copy[region][i_rp][i_stat] == mmm,
                                    mmm, mmm)
                    out_unit = {'1hr': 'mm hr-1'}
                    ds_copy[region][i_rp][i_stat].attrs['units'] = \
                        out_unit['1hr']
                if median == True:
                    ds_copy_median[region][i_rp][i_stat] = \
                        xr.where(ds_copy_median[region][i_rp][i_stat] == mmmedian,
                                    mmmedian, mmmedian)
                    out_unit = {'1hr': 'mm hr-1'}
                    ds_copy_median[region][i_rp][i_stat].attrs['units'] = \
                        out_unit['1hr']

    ### Add processing information
    proc_info = ('Returnperiods of maximum precipitation for different regions of '
                +'Switzerland. Following the methods ofBan, N., Rajczak, J.,'
                +' Schmidli, J. et al. Analysis of Alpine precipitation extremes '
                +'using generalized extreme value theory in convection-resolving '
                +'climate simulations. Clim Dyn 55, 61–75 (2020). '
                +'https://doi.org/10.1007/s00382-018-4339-4 . Multimodel median '
                +'value from a model ensemble including '
                + f'various institutes: [{Institutes}]. For more details please '
                + 'confer with https://gitlab.ethz.ch/lgimmi/ch2025-prototype' )
                
    if mean == True:
        ds_copy.attrs["processing_information"] = proc_info
        ### Save file
        filenmae_new = path_data + f'/EnsembleMean_{time_res}_RP_prmax_{season}.nc'
        # filenmae_new = path_data + f'/OneYearStep_EnsembleMean_{time_res}_RP_prmax_{season}.nc'
        ds_copy.to_netcdf(path=filenmae_new)
    if median == True:
        ds_copy_median.attrs["processing_information"] = proc_info
        ### Save file
        filenmae_new = path_data + f'/EnsembleMedian_{time_res}_RP_prmax_{season}.nc'
        # filename_new = path_data + f'/OneYearStep_EnsembleMedian_{time_res}_RP_prmax_{season}.nc'
        ds_copy_median.to_netcdf(path=filenmae_new)

    