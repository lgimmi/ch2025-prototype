#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Create Station Range values for different bootstrap numbers

"""

import os
import time
import pickle
import numpy as np
import pandas as pd
from multiprocessing import Pool
import rpy2.robjects as robjects
from rpy2.robjects import numpy2ri
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr

'''-------------------------------------------------------------------------'''
### evaluation, historical
scenario = 'evaluation'

time_res = '1hr'

# number_of_bootstraps = 50
which_bootstrap = np.arange(165,305,5)

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)

path_save = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_maxima/'
             +f'{scenario}')
path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
'''-------------------------------------------------------------------------'''

### Select time period of interest and change time dataformat for simpler use
# file = '/users/lgimmi/MeteoSwiss/data/rre150h0_1981_2022_allstats.csv'
if time_res == '1hr':
    file = ('/users/lgimmi/MeteoSwiss/data/rre150h0_1995_2010_allstats_'
            +'andpartnerstats_withNA.csv')
elif time_res == '1d':
    file = ('/users/lgimmi/MeteoSwiss/data/rre150d0_2000_2009_'
            +'allstatswithpartners_allNA.csv')

data = pd.read_csv(file)

if scenario == 'evaluation':
    start_time = 200001010000
    end_time = 200912312300
    years = np.arange(2000,2011,1)
else:
    start_time = 199601010000
    end_time = 200512312300
    years = np.arange(1996,2007,1)

mask = (data['time'] > start_time) & (data['time'] <= end_time)
data = data.loc[mask]
data.to_csv(f'/users/lgimmi/MeteoSwiss/data/{scenario}_station_data.csv')
data['time'] = pd.to_datetime(data['time'], format='%Y%m%d%H%M')

### Needed to install 'gevXpgd' module as it is not a standard package 
### included in rpy2
if not robjects.packages.isinstalled("gevXgpd"):
    utils = importr("utils")
    utils.install_packages("remotes", repos="https://cloud.r-project.org")
    remotes = importr("remotes")
    remotes.install_github("C2SM/gevXgpd")

### import R module as variable -> can now be used like a common function
gevXgpd = importr("gevXgpd")


# '''-----------------------------------------------------------'''
### Functions to be used to calculate everything. A summary of the python files
### used to calculate GEV values for climate model data.
maxima_num = 1

def independent_maxima(array):
    '''
    Returns maxima of time axis of input 3D-array and changes input
    array to ensure indpendence for next maxima (removes maxima
    that were found + two days before and after). Works with 1D (time) arrays.
    Returns maxima and altered array.

    ''' 
    t_beg = time.time()

    if time_res == '1hr':
        twoday = 48
    if time_res == '1d':
        twoday = 2
    
    try:
        ind_array = np.nanargmax(array, axis = 0, keepdims = True)
    except ValueError:
        breakpoint()

    max_array = np.nanmax(array, axis = 0, keepdims = True)
    global maxima_ten_year
    maxima_ten_year.append(max_array)
    
    ### Exclude maxima and values two days before and after max value
    for i_erase in range(twoday):
        try:
            np.put_along_axis(array, ind_array + i_erase, 
                            0.0, axis = 0)
        except IndexError:
            continue

        try: 
            np.put_along_axis(array,ind_array - i_erase,
                            0.0, axis = 0)
        except IndexError:
            continue
            
    global maxima_num
    # print(f'Time for calculating maxima {maxima_num}: ' + 
    #     '%.2f' % (time.time() - t_beg) + ' s')
    maxima_num+=1

    return array, max_array

def sel_seasons(df):
    '''
    Select seasons from Dataframe. Returns Dataframe
    '''
    global season
    if season == 'DJF':
        months = [12, 1, 2]
    elif season == 'MAM':
        months = [3, 4, 5]
    elif season == 'JJA':
        months = [6, 7, 8]
    elif season == 'SON':
        months = [9, 10, 11]

    df = df[df['time'].dt.month.isin(months)]

    return df

def sel_year(df):
    '''
    Selects year of interest from Dataframe. Special cases for 'DJF' season as 
    it includes data over two different years. Returns Dataframe.
    '''
    global year
    if season == 'DJF' and i_year == 0:
        months = [1, 2]
        df = df[df['time'].dt.month.isin(months)]
        df_year = df[df['time'].dt.year == year]
    
    elif season == 'DJF' and i_year == 10:
        months = 12
        df = df[df['time'].dt.month == months]
        df_year = df[df['time'].dt.year == year-1]

    elif season == 'DJF' and i_year != 0 and i_year != 10:
        df_year = df[df['time'].dt.strftime('%Y-%m').isin(
            [f'{year-1}-12', f'{year}-01', f'{year}-02'])]
        
    else:
        df_year = df[df['time'].dt.year == year]
    
    return df_year

def bootstrap_stations(data):
    '''
    Returns GEV return period values for individual station maxima with
    additional 50 bootstrap maxima arrays.

    '''
    n_bootstraps = number_of_bootstraps
    empty_array = np.empty(n_bootstraps).tolist()

    # t_beg = time.time()
    np.random.seed(7) 

    station_og = data[:]
    if all(pd.isnull(station_og)):
        breakpoint()
    for iteration in range(n_bootstraps):
        GEV_output = None
        safety = 0
        while GEV_output is None and safety <= 10:
            ### Somtimes np.random_choice creates singluar matrix
            ### systems which won't work for GEV function -> redo
            try:
                safety+=1
                if iteration == 0:
                    gridcell = station_og
                else:
                    gridcell = np.random.choice(station_og,size=30)
        
                ### Remove nan values as gevXgpd cannot take nan values
                gridcell = gridcell[np.logical_not(pd.isnull(gridcell))]
                if len(gridcell) == 0:
                    breakpoint()
                    break

                ### Input has to be 1-D df with 'numeric' type column
                df = pd.DataFrame(gridcell, columns=['Column1'])
                df["Column1"] = pd.to_numeric(df["Column1"])

                ### Run GEV function
                GEV_output = gevXgpd.fitGEV(df,ret=return_periods)
            except:
                print('redo')
        if safety >= 10:
            continue
            
        ### Get values for return period from output
        empty_array[iteration] = GEV_output[5]

    ### Save files
    # try:
    #     rm_old_file = ('rm ' + path_save_GEV + 
    #         f'/GEV_{station}_{season}.npy')
    #     os.system(rm_old_file)
    # except:
    #     print('No old file to remove')

    # np.save(path_save_GEV + f'/GEV_{station}_{season}.npy',
    #         np.array(empty_array, dtype=object), allow_pickle=True)
    
    return empty_array

def rearange_reduce(data): 
    '''
    Rearanges input values and return 10, 90 percentile, mean and median for
    every return period.
    '''
    rp_check = 0
    rp_list = []
    rp_values = {}
    only_one_time = 0
    n_bootstraps = number_of_bootstraps
    rp_dict = {}
    rp_check = 0
    data = np.array(data)
    ### Rearange array to use all bootstrap sampels for stats
    for bootstrap in range(n_bootstraps):
        try:
            for rp in range(data[bootstrap].shape[0]):

                rp_value = data[bootstrap][rp][0]
                pr = data[bootstrap][rp][1]

                if rp_check == 0:
                    rp_dict[rp_value] = [pr]
                else:
                    rp_dict[rp_value] += [pr]

                if rp == (data[bootstrap].shape[0]-1):
                    rp_check += 1
        except:
            continue
    
    ### Do stats for every return period
    for key in rp_dict.keys():
        if only_one_time <= len(rp_dict.keys()):
            try:
                empty_array = np.full(
                    (4,data.shape[0],data.shape[1]),
                    np.nan).tolist()
            except:
                breakpoint()
            rp_values[key] = empty_array
            only_one_time+=1
        rp_values[key][0] = np.mean(rp_dict[key])
        rp_values[key][1] = \
            np.median(rp_dict[key])
        rp_values[key][2] = \
            np.percentile(rp_dict[key],10)
        rp_values[key][3] = \
            np.percentile(rp_dict[key],90)
    
    ### Save files
    # try:
    #     rm_old_file = 'rm ' + path_save_GEV+ f'/Values_{station}_{season}.npy'
    #     os.system(rm_old_file)
    # except:
    #     print('No old file to remove')

    # a_file = open(path_save_GEV + f'/Values_{station}_{season}.npy', "wb")
    # pickle.dump(rp_values, a_file)
    # a_file.close()

    return rp_values
    
# '''-----------------------------------------------------------'''
for number_of_bootstraps in which_bootstrap:
    print((f'{number_of_bootstraps}').center(60,'-'))
    for season in ['JJA']:

        df = sel_seasons(data)
        df_stations = pd.DataFrame()
        list_rearanged_GEV = []
        count_stations = 0
        t_beg_2 = time.time()
        for i, station in enumerate(df.keys()):
            t_beg = time.time()
            if i <= 2 or station == 'GVE.1' or station == 'SIO.1':
                continue
            if (scenario == 'evaluation' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL'
                ]):
                continue
            if (scenario == 'evaluation' and time_res == '1d' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL','BAC','BLA','BOV','BSW','BZL','CHX','CZG','GEMIR',
                'GWA','CLA','KER','LBG','CRO','LOE','MAU','NYO','EVL','OBW','PSB',
                'RIG','FRA','SET','GEBTQ','VDORN','WAW','ZHNIE','HER','ZZG','LEN',
                'LON','MUO','NEB','RHN','RIE','STF','TDO','THI','TILOD','TISOM',
                'UER','VDARP','VDAVS','VDCHX','VDHON','VDMOL','VDPEU','VDREN',
                'VIO','WAB','GRA','MDO','SCD','VAE','BEC','CEV','LAB','LAT','SZB',
                'ZOF'
                ]):
                continue
            if (scenario == 'historical' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                'TIBED','TIOLI'
                ]):
                continue
            # try:
            #     np.nanargmax(df[station].values, axis = 0, keepdims = True)
            # except ValueError:
            #     continue
            # print(station)
            count_stations += 1
            maxima_ten_year = []
            for i_year, year in enumerate(years):
                if season != 'DJF' and i_year == 10:
                    continue
                df_year = sel_year(df)
                station_data = df_year[station].values

                maxima_num = 1
                
                ### 'DJF' in the first year only considers January and February,
                ### only take 2 maxima. The last Year only uses December thus one 
                ### maxima.
                if season == 'DJF' and i_year == 0:
                    ### Get two maxima of season (January, February)
                    array_loop1, maxima_1 = independent_maxima(station_data)
                    array_loop2, maxima_2 = independent_maxima(array_loop1)
                elif season == 'DJF' and i_year == 10:
                    ### Get one maxima of season (December)
                    array_loop1, maxima_1 = independent_maxima(station_data)
                else:
                    ### Get three maxima of season
                    array_loop1, maxima_1 = independent_maxima(station_data)
                    array_loop2, maxima_2 = independent_maxima(array_loop1)
                    array_loop3, maxima_3 = independent_maxima(array_loop2)

            # '''-----------------------------------------------------------'''
            ### Masking dry areas (>15 maxima with <1mm/h)
            # '''-----------------------------------------------------------'''
            maxima_ten_year = np.squeeze(np.array(maxima_ten_year))
            dry_areas = np.count_nonzero(maxima_ten_year < 1)
            maxima_no_dry = np.where(dry_areas > 15, np.nan, maxima_ten_year)
            df_stations[station] = maxima_no_dry

            ### translates python np.arrays and pd.datframes to R style
            numpy2ri.activate()
            pandas2ri.activate()

            GEV_station = bootstrap_stations(maxima_no_dry)

            rearanged_GEV = rearange_reduce(GEV_station)
            list_rearanged_GEV.append(rearanged_GEV)

            
            print(f'Time for calculating station {station} GEV: ' + 
                '%.2f' % (time.time() - t_beg) + ' s')
        print(f'Time for calculating station GEV: ' + 
                '%.2f' % (time.time() - t_beg_2) + ' s')
        ### ------------------------------------------------------------------- ###
        path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
        filename_region = (path_region_stations + 
                        f'/stations_region_{scenario}_{time_res}.csv')
        station_region = pd.read_csv(filename_region)
        st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []
        for rp in return_periods:
            st1, st2, st3, st4 = [],[],[],[]
            for i_station, station in enumerate(station_region.keys()):
                # if station_region.values[0][i_station] != region and region != 'CH':
                #     continue
                # st1.append(station_data[i_station][rp][2])
                st2.append(list_rearanged_GEV[i_station][rp][3])
                # st3.append(station_data[i_station][rp][0])
                st4.append(list_rearanged_GEV[i_station][rp][1])
            
            # st_ten_perc.append(np.nanmean(st1))
            st_ninety_perc.append(np.nanmean(st2))
            # st_mean.append(np.nanmean(st3))
            st_median.append(np.nanmean(st4))
        Delta_Spread = (np.array(st_ninety_perc) - np.array(st_median))
        Rel_Delta_Spread = \
            ((np.array(st_ninety_perc) - np.array(st_median)) / np.array(st_median))

        path_save_GEV = '/scratch/snx3000/lgimmi/store/miscellaneous/station_bootstrap'
        rm_old_file = ('rm ' + path_save_GEV +
                        f'/Delta_Spread_{number_of_bootstraps}_{time_res}.npy')
        Rel_rm_old_file = ('rm ' + path_save_GEV +
                        f'/Relative_Spread_{number_of_bootstraps}_{time_res}.npy')
        os.system(rm_old_file)
        os.system(Rel_rm_old_file)
        np.save(path_save_GEV + f'/Delta_Spread_{number_of_bootstraps}_{time_res}.npy',
                np.array(Delta_Spread, dtype=object), allow_pickle=True)
        np.save(path_save_GEV + f'/Relative_Spread_{number_of_bootstraps}_{time_res}.npy',
                np.array(Rel_Delta_Spread, dtype=object), allow_pickle=True)
# print(count_stations)
### Run all seasons at the same time 
# with Pool() as pool:
#     t_beg = time.time()
#     pool.starmap(stations_maxima, enumerate(['DJF', 'MAM', 'JJA', 'SON']))
#     print(f'Time for calculating station maxima: ' + 
#             '%.2f' % (time.time() - t_beg) + ' s')